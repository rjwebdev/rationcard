<!DOCTYPE html>
<html class="loading" lang="en" >
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="www.theanimator.in">
  <title>Ration distributors</title>
 <link rel="apple-touch-icon" sizes="57x57" href="assets/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="assets/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="assets/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="assets/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="assets/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="assets/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="assets/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="assets/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="assets/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="assets/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="assets/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="assets/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="assets/favicon/favicon-16x16.png">



  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700" rel="stylesheet">
  <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/vendors.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/icheck.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/custom.css">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/app.css">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/vertical-compact-menu.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/palette-gradient.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/login-register.css">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
<script>
    window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 4000);
    </script>
</head>
<body class="vertical-layout vertical-compact-menu 1-column  bg-full-screen-image menu-expanded blank-page blank-page" data-open="click" data-menu="vertical-compact-menu" data-col="1-column">

  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <section class="flexbox-container">
          <div class="col-12 d-flex align-items-center justify-content-center">
            <div class="col-md-4 col-10 box-shadow-2 p-0">
              <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                <div class="card-header border-0">
                    <div class="card-title text-center" >
                        
                      <img src="<?php echo base_url(); ?>assets/img/rationlogo.png" alt="">
                  </div>
                  <h6 class="card-subtitle  text-muted text-center  pt-2" style="font-size: 23px;color: #000;font-weight: bold;">
                      <span style="color:#00bcd4;">गजानन प्रसाद धान्य भंडार </span><br/><span>U/UMU 39 F144</span>
                  </h6>
                </div>
                <div class="card-content">
                  
                  <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1">
                      <span style="font-weight: bolder;">Welcome to Ration Card Management</span>
                  </p>
                   <div class="alert alert-success " role="alert">
                   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                   </button>
                <strong>Plz Login again</strong> 
                    <?php
                    if(!empty($success_msg)){
                        echo '<p class="statusMsg">'.$success_msg.'</p>';
                    }elseif(!empty($error_msg)){
                        echo '<p class="statusMsg">'.$error_msg.'</p>';
                    }
                    ?>
               </div>
                  <div class="card-body">
                    <form class="form-horizontal" action=""  method="POST" novalidate>
                        
                      <fieldset class="form-group position-relative has-icon-left">
                        <input type="text" class="form-control" id="username" name="username" placeholder="Your Username" value="<?php echo !empty($user['username'])?$user['username']:''; ?>" required>
                        <div class="form-control-position">
                           <i class="fa fa-user"></i>
                        </div>
                      </fieldset>
                        
                      <fieldset class="form-group position-relative has-icon-left">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password"
                        required>
                        <div class="form-control-position">
                          <i class="la la-key"></i>
                        </div>
                      </fieldset>
                      <div class="form-group row">
                        <div class="col-md-6 col-12 text-center text-sm-left">
                          <fieldset>
                            <input type="checkbox" id="remember-me" class="chk-remember">
                            <label for="remember-me"> Remember Me</label>
                          </fieldset>
                        </div>
                        <div class="col-md-6 col-12 float-sm-left text-center text-sm-right">
                            <a href="" class="card-link">Forgot Password?</a></div>
                      </div>
                        <input type="submit" name="loginSubmit" class="btn btn-outline-info btn-block" value="Login">
                    </form>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>

  <script src="<?php echo base_url();?>assets/js/vendors.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/js/jqBootstrapValidation.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/js/icheck.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/js/app-menu.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/js/app.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/js/form-login-register.js" type="text/javascript"></script>

</body>
</html>