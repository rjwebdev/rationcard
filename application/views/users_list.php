<?php require 'header.php';?>
<script>
    window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 4000);
    </script>
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0">Admin Users</h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>user">user</a>
                </li>
                <li class="breadcrumb-item"><a href="#">Login Data</a>
                </li>
                <li class="breadcrumb-item active">List Users
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-header-right text-md-right col-md-6 col-12">
          <div class="btn-group" data-toggle="tooltip" data-placement="top" title="Add New Employee">
              <a href="<?php echo base_url(); ?>user/add_employee"  class="btn btn-round btn-info" onclick="return addEmployee();"><i class="la la-user"></i> Add Employee</a>
          </div>
        </div>
      </div>
        <!--//card layout-->
        <div class="content-body">

        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                  <div class="card-header bg-panel" style="">
                    <h4 class="card-title">
                        <i class="la la-users" ></i> || All Active Employee List </h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    
                      <div class="card-text">
                          <?php if($feedback = $this->session->flashdata('feedback')):
			             $feedback_class = $this->session->flashdata('feedback_class');
	                         ?>
                      <div class="alert alert-icon-right <?php echo $feedback_class; ?> mb-2" role="alert">
                        <span class="alert-icon"><i class="la la-info"></i></span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                        <strong> <?php echo $feedback; ?></strong>
                      </div>
                          <?php endif; ?>
                    </div>

                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th>U.Id</th>
                          <th>U.Name</th>
                          <th>U.Position</th>
                          <th>U.Phone</th>
                          <th>U.Adds</th>
                          <th>U.Action</th>
                        </tr>
                      </thead>
                      <tbody>
                           <?php if(count($admin_user) ): 
                                          $count=1;
			                   foreach($admin_user as $admin_user):?>
                        <tr>
                            <td style="width: 2%"><span class="badge badge-pill badge-danger">U.ID-00<?php echo $count++; ?></span> </td>
                          <td> <?php echo $admin_user->emp_fname ?> <?php echo $admin_user->emp_lname; ?> </td>
                          <td> <?php echo $admin_user->role_name; ?> <small class="primary darken-1">[<?php echo $admin_user->emp_email; ?>] </small></td>
                          <td><?php echo $admin_user->emp_phone; ?></td>
                            <td><?php echo $admin_user->emp_adds; ?></td>
                            <td>
                                
                             <div class="btn-group mx-2" role="group" aria-label="Second Group" >
                             <button type="button" class="btn btn-icon btn-info" data-toggle="tooltip" data-placement="top" title="View"><i class="la la-eye"></i></button>
                             
                             <a  class="btn btn-icon btn-success" href="<?php echo base_url("user/edit_employee/$admin_user->emp_id") ?>" onclick="return updateemployee();" data-toggle="tooltip" data-placement="top" title="Edit">
                                                                   <i class="la la-edit"></i>
                                                                  </a>
                              <!--<button type="button" class=""></button>-->
                             
                              <?=	
				  form_open('user/delete_employee'),
				  form_hidden('emp_id',$admin_user->emp_id),
                                  form_button(array('name' => 'submit', 'data-toggle'=>'tooltip','placement'=>'top','title'=>'Delete','type' => 'submit', 'class' => 'btn btn-icon btn-warning', 'onclick'=>'return deleteemployee();','content' => '<i class="la la-trash-o"></i>')),
//						form_submit(array('name'=>'submit','value'=>'','class'=>'btn btn-danger ','content' => '<i class="fa fa-search fa-fw"></i> ')),	
						form_close();
					      ?>
                            </div>
                                
                                
                            </td>
                        </tr>
                          <?php endforeach; ?>
                                            <?php else: ?>

                                            <?php endif; ?>
                      </tbody>
                      
                    </table>
                      
                      
                      
                      
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
       <!--//end card body-->
        </div>
      <!--//end content wrapper-->  
    </div>
</div>   
   <script>
    function addEmployee(){
    job=confirm("Are you sure you want to Add new Employee?");
    if(job!=true){
         document.location.reload(true);
        return false;
    }
   }
 </script>
<?php require 'footer.php';?>