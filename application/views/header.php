<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
  
 <title>Ration distributors</title>
 <link rel="apple-touch-icon" sizes="57x57" href="assets/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="assets/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="assets/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="assets/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="assets/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="assets/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="assets/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="assets/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="assets/favicon/apple-icon-180x180.png">

  
  <!--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700" rel="stylesheet">-->
  <link href="<?php echo base_url(); ?>assets/css/line-awesome.min.css" rel="stylesheet">
   <link href="<?php echo base_url(); ?>assets/css/fontcss.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/vendors.css">
  <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/datatable/dataTables.bootstrap4.min.css">-->
  <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/datatable/rowReorder.dataTables.min.css">-->
  <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/datatable/responsive.dataTables.min.css">-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/datatables.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/app.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/vertical-compact-menu.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/palette-gradient.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/fonts/feather/style.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/ui_icons.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/users.css">
<style>
    .bg-panel{
        background-color: #d9f2f3;
    }
</style>
</head>
<body class="vertical-layout vertical-compact-menu 2-columns   menu-expanded fixed-navbar"
data-open="click" data-menu="vertical-compact-menu" data-col="2-columns">
  <!-- fixed-top-->
  <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-dark bg-cyan navbar-shadow navbar-brand-center">
    <div class="navbar-wrapper">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
          <li class="nav-item mobile-menu d-md-none mr-auto">
              <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#">
                  <i class="ft-menu font-large-1"></i></a>
          </li>
          <li class="nav-item">
              <a class="navbar-brand" href="<?php echo base_url(); ?>">
              <img class="brand-logo" alt="" src="<?php echo base_url();?>assets/img/homerationlogo2.png">
              <h3 class="brand-text">गजानन प्रसाद धान्य भंडार [<small style="color:fff"> U/UMU 39 F144 </small> ]</h3>
            </a>
          </li>
          <li class="nav-item d-md-none">
            <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="la la-ellipsis-v"></i>
            </a>
          </li>
        </ul>
      </div>
      <div class="navbar-container content">
        <div class="collapse navbar-collapse" id="navbar-mobile">
          <ul class="nav navbar-nav mr-auto float-left">
            <li class="nav-item d-none d-md-block">
                <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a>
            </li>
            <li class="nav-item d-none d-md-block">
                <a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a>
            </li>
            
            <li class="nav-item nav-search">
                <a class="nav-link nav-link-search" href="#"><i class="ficon ft-search"></i></a>
              <div class="search-input">
                <input class="input" type="text" placeholder="search">
              </div>
            </li>
          </ul>
          <ul class="nav navbar-nav float-right">
            <li class="dropdown dropdown-user nav-item" >
              <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                <span class="mr-1">
                    <span class="user-name text-bold-700" style="text-transform: capitalize;"><?php echo $this->session->userdata('name','name');?></span>
                </span>
                <span class="avatar avatar-online" data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Profile" >
                    <img src="<?php echo base_url();?>assets/img/boy.png" alt="avatar"><i></i>
                </span>
              </a>
              <div class="dropdown-menu dropdown-menu-right">
                  <a class="dropdown-item" href="#"><i class="ft-user"></i> Edit Profile</a>
                <a class="dropdown-item" href="<?php echo base_url();?>admin/user_profile"><i class="ft-mail"></i> Change Password </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?php echo site_url();?>admin/logout"> <i class="ft-power"></i> Logout</a>
              </div>
            </li>
            
            <li class="dropdown dropdown-notification nav-item">
              <a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="ficon ft-bell"></i>
                <span class="badge badge-pill badge-default badge-danger badge-default badge-up badge-glow"><?php echo $this->session->userdata('role_name','role_name');?></span>
              </a>
              <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                <li class="dropdown-menu-header">
                  <h6 class="dropdown-header m-0">
                      <span class="grey darken-2"><span style="color:#003399"><?php echo $this->session->userdata('role_name','role_name');?> </span> Notifications</span>
                  </h6>
                  <span class="notification-tag badge badge-default badge-danger float-right m-0">New</span>
                </li>
                <li class="scrollable-container media-list w-100">
                  <a href="javascript:void(0)">
                    <div class="media">
                      <div class="media-left align-self-center"><i class="ft-plus-square icon-bg-circle bg-cyan"></i></div>
                      <div class="media-body">
                        <h6 class="media-heading">You have new feature!</h6>
                        <p class="notification-text font-small-3 text-muted">Coming soon</p>
                        <small>
                          <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00"><?php echo date('l jS \of F Y h:i:s A'); ?></time>
                        </small>
                      </div>
                    </div>
                  </a>
                </li>
                <li class="dropdown-menu-footer">
                    <a class="dropdown-item text-muted text-center" href="javascript:void(0)"></a>
                </li>
              </ul>
            </li>
            <li class="dropdown dropdown-notification nav-item">
              <a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="ficon ft-mail">             </i></a>
              <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                <li class="dropdown-menu-header">
                  <h6 class="dropdown-header m-0">
                    <span class="grey darken-2">coming soon</span>
                  </h6>
                  <span class="notification-tag badge badge-default badge-warning float-right m-0">New</span>
                </li>
                <li class="scrollable-container media-list w-100">
                  <a href="javascript:void(0)">
                    <div class="media">
                      <div class="media-left">
                        <span class="avatar avatar-sm avatar-online rounded-circle">
                        <i></i></span>
                      </div>
                      <div class="media-body">
                        <h6 class="media-heading">coming soon</h6>
                        <p class="notification-text font-small-3 text-muted"><?php echo date('l jS \of F Y h:i:s A');?></p>
                        
                      </div>
                    </div>
                  </a>
                  
                </li>
                <li class="dropdown-menu-footer">
                    <a class="dropdown-item text-muted text-center" href="javascript:void(0)"></a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>

  <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="main-menu-content">
        
      <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
         <?php if($this->session->userdata('role_name') == "Admin" || $this->session->userdata('role_name') == "Employee" ): ?>
          <li class=" nav-item">
            <a href="<?php echo base_url(); ?>"><i class="la la-home"></i>
              <span class="menu-title" data-i18n="nav.dash.main">Dashboard</span></a> 
         </li>
           <?php endif; ?> 
          <?php if($this->session->userdata('role_name') == "Admin" ): ?>
        <li class=" nav-item">
            <a href="<?php echo base_url(); ?>admin_user"><i class="la la-users"></i>
                <span class="menu-title" data-i18n="nav.templates.main">Employee</span>
            </a>
        </li>
         <?php endif; ?> 
        <?php if($this->session->userdata('role_name') == "Employee" || $this->session->userdata('role_name') == "Admin" ): ?>
        <li class=" nav-item">
            <a href="<?php echo base_url(); ?>cardholder"><i class="la la-file-text"></i>
                <span class="menu-title" data-i18n="nav.templates.main">card Holder</span>
            </a>
        </li>
         <?php endif; ?> 
        
        <li class=" nav-item">
            <a href="#">
                <i class="la la-navicon"></i>
                <span class="menu-title" data-i18n="">Reposts</span></a>
          <ul class="menu-content">
            <li><a class="menu-item" href="<?php echo base_url(); ?>reports" data-i18n="nav.navbars.nav_light">Months Reports</a>
            </li>
            <li><a class="menu-item" href="<?php echo base_url(); ?>reports/purchase_Report" data-i18n="nav.navbars.nav_dark">All Purchase Report</a></li>
          </ul>
        </li>
        <li class=" nav-item">
            <a href="<?php echo base_url();?>admin/user_profile"><i class="la la-gear"></i>
                <span class="menu-title" data-i18n="nav.templates.main">Setting</span>
            </a>
        </li>

      </ul>
        
    </div>
  </div>