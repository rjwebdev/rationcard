<?php require 'header.php';?>
<style>
 .title{
  text-align: center;
  color: #2c3e50;
}
.container {max-width: 940px; width: 100%; margin: 0 auto; } 
.twelve { width: 100%; } 
.eleven { width: 91.53%; } 
.ten { width: 83.06%; } 
.nine { width: 74.6%; } 
.eight { width: 66.13%; } 
.seven { width: 57.66%; } 
.six { width: 49.2%; } 
.five { width: 40.73%; } 
.four { width: 32.26%;} 
.three { width: 23.8%; } 
.two { width: 15.33%; } 
.one{ width: 6.866%;}
.col { 
	display: block; 
	float:left;
	margin: 1% 0 1% 1.6%; 
} 

.col:first-of-type { margin-left: 0; }
 .cf:before, .cf:after { 
 	content: " "; 
 	display: table; 
 } 
#card2{
	overflow: hidden;
	color: #2c3e50;
}
#card2 a{
	color: #2c3e50;
}
#card2 .wrapper{
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

#card2 .header1{
	width: 100%;
	height: 100px;
	position: relative;
	background-color: #3498db;
}
#card2 .header1:after{
    content: '';
    background: inherit;
    bottom: 0;
    display: block;
    height: inherit;
    position: absolute;
    left: 0;
    right: 0;
    transform: skewY(-8deg);
    -webkit-transform: skewY(-8deg);
    transform-origin: 100%;
}
#card2 .image-wrapper{
	width: 100px;
	height: 100px;
	border-radius: 100%;
        background-image: url("<?php echo base_url();?>assets/img/boy.png");
	background-size: cover;
	background-repeat: no-repeat;
	position: absolute;
    z-index: 5;
    top: 130px;
    left: 50%;
    border:1px solid #ccc;
    margin-left: -50px;
}
#card2 .name{
	margin-top: 150px;
	text-align: center;
}
#card2 .social .four{
	text-align: center;
}
#card2 .info{
	margin: 30px 0;
	text-align: center;
}
    </style>
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
            <!--//card layout-->
            <section id="" class="" style="padding-top:30px;">
            <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header bg-panel">
                  <h4 class="card-title" id="horz-layout-icons">Timesheet</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                </div>
                <div class="card-content collpase show">
                  <div class="card-body">
                      <div class="row">
                          <div id="card2" class="card col-md-4">
                      <div class="header1" style="border-top:3px solid #f44336;"></div>
                        <div class="wrapper">
                      <div class="image-wrapper"></div>
                     <h3 class="name">
                         <?php echo $this->session->userdata('name','name');?> <br/>
                         <span style="color:#f44336"> [ <?php echo $this->session->userdata('role_name','role_name');?> ]</span>
                     </h3>
      </div>
    </div>
               <div class="col-md-8">
                    <div class="card">
                   <div class="card-header bg-info">
                       <h4 class="card-title " style="color:#FFF;" id="horz-layout-icons"> Change Password </h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    
                  </div>
                </div>
               <div class="card-content collpase show">
                  <div class="card-body">
                  <?php echo form_open_multipart('admin/update'); ?>
                      <div class="form-body">
                        <div class="form-group row">
                          <label class="col-md-3 label-control" for="">Old Password</label>
                          <div class="col-md-9">
                            <div class="position-relative has-icon-left">
                                <input type="password" required="" id="oldPassword" name="oldPassword" class="form-control" placeholder="Old Password here" >
                              <div class="form-control-position">
                                <i class="ft-user"></i>
                              </div>
                                <span class="text-danger"><?php echo form_error('oldPassword'); ?></span>
                            </div>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-md-3 label-control" for="">New Password</label>
                          <div class="col-md-9">
                            <div class="position-relative has-icon-left">
                                <input type="password" required="" id="newPassword" name="newPassword" class="form-control" placeholder="New Password here">
                              <div class="form-control-position">
                                <i class="la la-briefcase"></i>
                              </div>
                                <span class="text-danger"><?php echo form_error('newPassword'); ?></span>
                            </div>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-md-3 label-control" for="">Confirm Password</label>
                          <div class="col-md-9">
                            <div class="position-relative has-icon-left">
                                <input type="password" required="" id="renewPassword" name="renewPassword" class="form-control" placeholder="Confirm password">
                              <div class="form-control-position">
                                <i class="ft-message-square"></i>
                              </div>
                                <span class="text-danger"><?php echo form_error('renewPassword'); ?></span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-actions right">
                        <button type="button" class="btn btn-warning mr-1">
                          <i class="ft-x"></i> Cancel
                        </button>
                        <button type="submit" class="btn btn-primary">
                          <i class="la la-check-square-o"></i> Save
                        </button>
                      </div>
                    </form>
                  </div>
                        </div>
                          </div>
               </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
                </section>
            
        
      </div>
    </div>
  </div>
  </div>

<?php require 'footer.php';?>