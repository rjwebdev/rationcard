<?php require 'header.php';?>

<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0">Admin Users</h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin.user">user</a>
                </li>
                <li class="breadcrumb-item"><a href="#">Login Data</a>
                </li>
                <li class="breadcrumb-item active">Add User
                </li>
              </ol>
            </div>
          </div>
        </div>
        
      </div>
        <!--//card layout-->
        <div class="content-body">

        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                  <div class="card-header bg-panel" style="">
                    <h4 class="card-title">
                        <i class="la la-user-plus" ></i> || Add New User
                    </h4>
                 </div>
                <div class="card-content collapse show">
                  <div class="card-body ">
                      

                      
                    <?php $attributes = array('id' => 'employeeform'); echo form_open_multipart('user/store_employee',$attributes);?>
                            <?php echo form_hidden('created_on',  date('Y-m-d H:i:s')); ?>

                      <div class="form-body">
                          <div class="col-md-9 offset-md-1">
                        <h4 class="form-section"><i class="ft-user"></i> Add Personal Info</h4>
                        <div class="row">
                            
                          <div class="form-group col-md-6 mb-2">
                             <label for="">First Name</label>
                            <div class="position-relative has-icon-left">
                                <input type="text" id="" class="form-control" required="" placeholder="First Name"  name="emp_fname">
                              <div class="form-control-position">
                                <i class="ft-user"></i>
                              </div>
                            </div>
                          </div>
                          <div class="form-group col-md-6 mb-2">
                            <label for="">Last Name</label>
                            <div class="position-relative has-icon-left">
                                <input type="text" id="" class="form-control"  required="" placeholder="Last Name"  name="emp_lname">
                              <div class="form-control-position">
                                <i class="ft-user"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6 mb-2">
                            <label for="">Address</label>
                            <div class="position-relative has-icon-left">
                                <input type="text" id="" class="form-control" required="" placeholder=""  name="emp_adds">
                              <div class="form-control-position">
                                <i class="ft-home"></i>
                              </div>
                            </div>
                          </div>
                          <div class="form-group col-md-6 mb-2">
                            <label for="">Contact Number</label>
                            <div class="position-relative has-icon-left">
                              <input type="text" id="" class="form-control" placeholder="Contact Numbers"  name="emp_phone">
                              <div class="form-control-position">
                                <i class="ft-phone-call"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        <h4 class="form-section"><i class="ft-clipboard"></i> Login Info</h4>
                        <div class="row">
                          <div class="form-group col-6 mb-2">
                            <label for="">Email ID</label>
                             <div class="position-relative has-icon-left">
                                 <input type="email" id="" class="form-control" required="" placeholder="Email ID" name="emp_email">
                            <div class="form-control-position">
                            <i class="la la-envelope-o"></i>
                              </div>
                            </div>
                          </div>
                            <div class="form-group col-6 mb-2">
                            <label for="">Password</label>
                             <div class="position-relative has-icon-left">
                                 <input type="password" id="" class="form-control" required="" placeholder="Password" name="password">
                            <div class="form-control-position">
                              <i class="la la-unlock-alt"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 mb-2">
                            <label for="">Position</label>
                            <div class="position-relative has-icon-left">
<!--                                <input type="text" id="" class="form-control" required="" placeholder="Employee Position"  name="eposition">-->
                                <select class="form-control show-tick" name="eposition" required >
                                        <option value="">-- Please Select Role --</option>
                                            <?php if(count($role)):?>
                                             <?php foreach ($role as $role):?>
                                                 <option value="<?php echo $role->role_id;?>"> <?php echo $role->role_name;?></option>
                                            <?php endforeach;?>
                                             <?php else:?>
                                            <?php endif;?>
                                    </select>
                              <div class="form-control-position">
                                <i class="ft-user-x"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                          </div>
                        <!--//footer page-->
                        <div class="col-md-9 offset-1 ">
                            
                         <?php 
				   echo form_reset(array('name'=>'reset','value'=>'RESET','class'=>'btn btn-warning mr-1')),
					form_submit(array('name'=>'submit','value'=>'ADD EMPLOYEE','class'=>'btn btn-primary','onclick'=>' return addemployee();'));
			              ?>
                            
                      </div>
                           <?php form_close(); ?>

                    <!--//card boy close-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
       <!--//end card body-->
        </div>
      <!--//end content wrapper-->  
    </div>
</div>

 <script type="text/javascript">
        
  function confimADD(){
    job=confirm("Are you sure to Reset?");
    if(job!=true){
       document.location.reload(true);
        return false;
    }
   }
 </script>
<?php require 'footer.php';?>