<?php require 'header.php';?>
<script>
    window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 4000);
    </script>
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0">Monthly Reports</h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="">Reports</a>
                </li>
                <li class="breadcrumb-item"><a href="#">Login Data</a>
                </li>
                <li class="breadcrumb-item active">List 
                </li>
              </ol>
            </div>
          </div>
        </div>
        
      </div>
        <!--//card layout-->
        <div class="content-body">

        <section id="configuration">
          <div class="row">
            <div class="col-12">
                             
                <!--///new tab-->
                <div class="card">
                <div class="card-header bg-panel" style="">
                    <h4 class="card-title">
                        <i class="la la-users" ></i> || All Reports </h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content">
                  <div class="card-body">
                   
                    <ul class="nav nav-tabs nav-justified nav-top-border no-hover-bg">
                      <li class="nav-item">
                        <a class="nav-link active" id="active-tab" data-toggle="tab" href="#active" aria-controls="active"
                           aria-expanded="true"> <i class="la la-files-o"></i> All Monthly Visiter </a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" id="link-tab" data-toggle="tab" href="#link" aria-controls="link" aria-expanded="false"> All Cardholder </a>
                      </li>
                      <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                        aria-haspopup="true" aria-expanded="false">
								Dropdown
								</a>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" id="dropdownOpt1-tab" href="#dropdownOpt1" data-toggle="tab"
                          aria-controls="dropdownOpt1" aria-expanded="true">dropdown 1</a>
                          <a class="dropdown-item" id="dropdownOpt2-tab" href="#dropdownOpt2" data-toggle="tab"
                          aria-controls="dropdownOpt2" aria-expanded="true">dropdown 2</a>
                        </div>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="linkOpt-tab" data-toggle="tab" href="#linkOpt" aria-controls="linkOpt">Another Link</a>
                      </li>
                    </ul>
                    <div class="tab-content px-1 pt-1">
                      <div role="tabpanel" class="tab-pane active" id="active" aria-labelledby="active-tab"
                      aria-expanded="true">
                        <!--//monthly data-->
                          <div class="card-body card-dashboard">
                       
                      <div class="col-md-12">
                        
                        <div class="row">
                           
                          <div class="form-group col-md-3 mb-2">
                             <label for="">Select Start Date</label>
                            <div class="position-relative has-icon-left">
                                <input placeholder="From Date" class="form-control "  name="fromDate" type="date"  value="<?php echo $this->input->post('fromDate') ?>" id="fromDate"> 
                              <div class="form-control-position">
                                <i class="ft-calendar"></i>
                              </div>
                            </div>
                          </div>
                          <div class="form-group col-md-3 mb-2">
                            <label for="">Select End Date</label>
                            <div class="position-relative has-icon-left">
                               <input placeholder="To Date" class="form-control datepicker" id="toDate" name="toDate" type="text" onfocus="(this.type='date')"  value="<?php echo isset($data->date)?date("Y-m-d",strtotime($data->date)):date("Y-m-d",  strtotime("now")); ?>" > 
                              <div class="form-control-position">
                                <i class="ft-calendar"></i>
                              </div>
                            </div>
                          </div>
                            <div class="form-group col-md-3 mb-2">
                            <label for="">-</label>
                            <div class="position-relative has-icon-left">
                              <button type="submit" name="submit" id=""  class="btn  btn-success">Search</button>  
                              
                            </div>
                          </div>
                            <div class="form-group col-md-3 mb-2">
                                <label for=""><i class="fa fa-file-excel-o" ></i> Cardholder Visited Report</label>
                            <div class="position-relative has-icon-left">
                             <a href="<?php echo base_url(); ?>cardholder/vexport"  onclick="" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Export Months Visited Data">  Export report </a>  
                              
                            </div>
                          </div>
                          
                        </div>
                         <?php echo form_close(); ?>
                          
                          
                      </div>
             
                    <div id="suggestions" class="table-responsive">
                        
                      <table id="" class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                            <th hidden="">#Id</th>
                            <th> संदर्भ क्रमांक </th>
                          <th> बारकोड क्रमाँक </th>
                          <th> कार्डधारक नाव </th>
                          <th> मोबाइल नंबर </th>
                          <th> कार्ड प्रकार </th>
                          <!--<th> तारीख </th>-->
                          <th>Visit date</th>
                          <!--<th>U.Action</th>-->
                        </tr>

                      </thead>
                      <tbody id="">
                           <?php if(count($monthly_visit) ): 
                                          $count=1;
			                  foreach($monthly_visit as $monthly_visit):?>
                          <tr style="font-size:13px;" id="autoSuggestionsList">
                            <td style="width: 2%" hidden=""><?php echo $count++; ?> </span></td>
                            <td> <span class="badge badge-pill badge-primary">
                                <?php echo $monthly_visit->rel_id ?> </span>
                            </td>
                            <td> <?php echo $monthly_visit->barcode_no; ?> </td>
                            <td><?php echo $monthly_visit->c_name; ?></td>
                            <td><?php echo $monthly_visit->c_mobile; ?></td>
                         
                            <td><?php echo $monthly_visit->c_cardtype; ?></td>
                            <td>
                                <?php  $dateform =  $monthly_visit->entry_date;
                                       $newDate = date("F j, Y", strtotime($dateform));
                                ?>
                                <span class="badge badge-pill badge-primary"><?php echo $newDate;?></span>
                            </td>
                            
                            
                        </tr>
                          <?php endforeach; ?>
                          <?php else: ?>

                      <?php endif; ?>
                      </tbody>
                   
                    </table>
                      </div>
                  </div>
                          
                      </div>
                      <div class="tab-pane" id="link" role="tabpanel" aria-labelledby="link-tab" aria-expanded="false">
                       <table id="" class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                            <th hidden="">#Id</th>
                            <th> संदर्भ क्रमांक </th>
                          <th> बारकोड क्रमाँक </th>
                          <th> कार्डधारक नाव </th>
                          <th> मोबाइल नंबर </th>
                          <th> कार्ड प्रकार </th>
                          <!--<th> तारीख </th>-->
                          <th>Visit date</th>
                          <!--<th>U.Action</th>-->
                        </tr>

                      </thead>
                      <tbody id="">
                           <?php if(count($monthly_visit) ): 
                                          $count=1;
			                  foreach($monthly_visit as $monthly_visit):?>
                          <tr style="font-size:13px;" id="autoSuggestionsList">
                            <td style="width: 2%" hidden=""><?php echo $count++; ?> </span></td>
                            <td> <span class="badge badge-pill badge-primary">
                                <?php echo $monthly_visit->rel_id ?> </span>
                            </td>
                            <td> <?php echo $monthly_visit->barcode_no; ?> </td>
                            <td><?php echo $monthly_visit->c_name; ?></td>
                            <td><?php echo $monthly_visit->c_mobile; ?></td>
                         
                            <td><?php echo $monthly_visit->c_cardtype; ?></td>
                            <td>
                                <?php  $dateform =  $monthly_visit->entry_date;
                                       $newDate = date("F j, Y", strtotime($dateform));
                                ?>
                                <span class="badge badge-pill badge-primary"><?php echo $newDate;?></span>
                            </td>
                            
                            
                        </tr>
                          <?php endforeach; ?>
                          <?php else: ?>

                      <?php endif; ?>
                      </tbody>
                   
                    </table>
                      </div>
                      <div class="tab-pane" id="dropdownOpt1" role="tabpanel" aria-labelledby="dropdownOpt1-tab"
                      aria-expanded="false">
                        <p>Fruitcake marshmallow donut wafer pastry chocolate topping
                          cake. Powder powder gummi bears jelly beans. Gingerbread
                          cake chocolate lollipop. Jelly oat cake pastry marshmallow
                          sesame snaps.</p>
                      </div>
                      <div class="tab-pane" id="dropdownOpt2" role="tabpanel" aria-labelledby="dropdownOpt2-tab"
                      aria-expanded="false">
                        <p>Soufflé cake gingerbread apple pie sweet roll pudding. Sweet
                          roll dragée topping cotton candy cake jelly beans. Pie
                          lemon drops sweet pastry candy canes chocolate cake bear
                          claw cotton candy wafer.</p>
                      </div>
                      <div class="tab-pane" id="linkOpt" role="tabpanel" aria-labelledby="linkOpt-tab"
                      aria-expanded="false">
                        <p>Cookie icing tootsie roll cupcake jelly-o sesame snaps. Gummies
                          cookie dragée cake jelly marzipan donut pie macaroon. Gingerbread
                          powder chocolate cake icing. Cheesecake gummi bears ice
                          cream marzipan.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                
                
                
                
                
            </div>
          </div>
        </section>
       <!--//end card body-->
        </div>
      <!--//end content wrapper-->  
    </div>
</div>   
   <script>
    function addEmployee(){
    job=confirm("Are you sure you want to Add new Employee?");
    if(job!=true){
         document.location.reload(true);
        return false;
    }
   }
 </script>
<?php require 'footer.php';?>