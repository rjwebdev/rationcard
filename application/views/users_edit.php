<?php require 'header.php';?>

<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0">Admin Users</h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>user">user</a>
                </li>
                <li class="breadcrumb-item"><a href="#">Login Data</a>
                </li>
                <li class="breadcrumb-item active">Add User
                </li>
              </ol>
            </div>
          </div>
        </div>
        
      </div>
        <!--//card layout-->
        <div class="content-body">

        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                  <div class="card-header bg-panel" style="">
                    <h4 class="card-title">
                        <i class="la la-user-plus" ></i> || Add New User
                    </h4>
                 </div>
                <div class="card-content collapse show">
                  <div class="card-body ">
                      
<!--                      <div class="card-text">
                      <div class="alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                        <span class="alert-icon"><i class="la la-info"></i></span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                        <strong>Error alert</strong>
                      </div>
                    </div>-->
                      
                    <?php echo form_open("user/update_employee/$user->emp_id");?>

                      <div class="form-body">
                          <div class="col-md-9 offset-md-1">
                        <h4 class="form-section"><i class="ft-user"></i> Update Personal Info</h4>
                        <div class="row">
                            
                          <div class="form-group col-md-6 mb-2">
                             <label for="">First Name</label>
                            <div class="position-relative has-icon-left">
                                 <?php echo form_input(array('name'=>'emp_fname','class'=>'form-control','placeholder'=>'Employee FName','value'=>set_value('emp_fname',$user->emp_fname))) ?>
                              <div class="form-control-position">
                                <i class="ft-user"></i>
                              </div>
                            </div>
                          </div>
                          <div class="form-group col-md-6 mb-2">
                            <label for="">Last Name</label>
                            <div class="position-relative has-icon-left">
                              <?php echo form_input(array('name'=>'emp_lname','class'=>'form-control','placeholder'=>'Employee LName','value'=>set_value('emp_lname',$user->emp_lname))) ?>
                              <div class="form-control-position">
                                <i class="ft-user"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6 mb-2">
                            <label for="">Address</label>
                            <div class="position-relative has-icon-left">
                                <?php echo form_input(array('name'=>'emp_adds','class'=>'form-control','placeholder'=>'Employee Adds','value'=>set_value('emp_adds',$user->emp_adds))) ?>
                              <div class="form-control-position">
                                <i class="ft-home"></i>
                              </div>
                            </div>
                          </div>
                          <div class="form-group col-md-6 mb-2">
                            <label for="">Contact Number</label>
                            <div class="position-relative has-icon-left">
                              <?php echo form_input(array('name'=>'emp_phone','class'=>'form-control','placeholder'=>'Contact Numbers','value'=>set_value('emp_phone',$user->emp_phone))) ?>
                              <div class="form-control-position">
                                <i class="ft-phone-call"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        <h4 class="form-section"><i class="ft-clipboard"></i> Login Info</h4>
                        <div class="row">
                          <div class="form-group col-6 mb-2">
                            <label for="">Email ID</label>
                             <div class="position-relative has-icon-left">
                            <?php echo form_input(array('name'=>'emp_email','class'=>'form-control','placeholder'=>'Email ID','readonly'=>'','value'=>set_value('emp_email',$user->emp_email))) ?>
                            <div class="form-control-position">
                               <i class="la la-envelope-o"></i>
                              </div>
                            </div>
                          </div>
                            <div class="form-group col-6 mb-2">
                            <label for="">Password</label>
                             <div class="position-relative has-icon-left">
                                 <input type="password" id="" class="form-control" placeholder="Password" name="" value="************" readonly="">
                            <div class="form-control-position">
                                <i class="la la-unlock-alt"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 mb-2">
                            <label for="">Position</label>
                            <div class="position-relative has-icon-left">
                              <?php // echo form_input(array('name'=>'eposition','class'=>'form-control','placeholder'=>'Email ID','value'=>set_value('eposition',$user->eposition))) ?>
                              <select class="form-control autocomplete "    name="eposition" id="eposition"  >
                                                    <option  value="<?php echo $user->role_id; ?>" ><?php echo $user->role_name; ?></option>
                                                      <?php foreach($role as $key => $role) {?>
                                                            <option value="<?php echo $role->role_id; ?>"><?php echo $role->role_name; ?></option>";
                                                       <?php } ?>
                                                </select>
                                <div class="form-control-position">
                                <i class="ft-user-x"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                          </div>
                        <!--//footer page-->
                        <div class="col-md-9 offset-1 ">
                            
                            <?php 
				         echo form_reset(array('name'=>'reset','value'=>'RESET','class'=>'btn btn-warning mr-1','type'=>'submit')),
					form_submit(array('name'=>'submit','value'=>'UPDATE EMPLOYEE','class'=>'btn btn-primary','onclick'=>'return updateemployee();'));
			              ?>
                            <?php form_close(); ?>
                       
                      </div>
                         

                    <!--//card boy close-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
       <!--//end card body-->
        </div>
      <!--//end content wrapper-->  
    </div>
</div>


<?php require 'footer.php';?>