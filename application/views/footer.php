
  <footer class="footer footer-static footer-dark navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2018
          <a class="text-bold-800 grey darken-2" href=""
        target="_blank">theanimator.in </a>, All rights reserved. </span>
      <span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">Rakesh Jadhav <i class="ft-heart pink"></i></span>
    </p>
  </footer>

    <script src="<?php echo base_url();?>assets/js/vendors.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url();?>assets/js/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/jquery.repeater.min.js" type="text/javascript"></script>

 <script src="<?php echo base_url(); ?>assets/js/app-menu.js" type="text/javascript"></script>
 <script src="<?php echo base_url(); ?>assets/js/app.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/js/customizer.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/datatable-basic.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/form-repeater.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url(); ?>assets/js/navs.js" type="text/javascript"></script>-->
</body>
</html>