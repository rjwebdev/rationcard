<?php  require 'header.php';?>

<div class="app-content content">
    <div class="content-wrapper">
        
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0">Purchase Reports</h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="">Reports</a>
                </li>
                <li class="breadcrumb-item"><a href="#">Data</a>
                </li>
                <li class="breadcrumb-item active">List 
                </li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    <section>
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header bg-panel">
                    <h4 class="card-title"> <i class="la la-file-pdf-o"></i> || All Visiter Reports</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">

                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <div class="col-md-12">
                        <?php echo form_open('reports/purchase-report'); ?> 
                        <div class="row">
                           
                          <div class="form-group col-md-3 mb-2">
                             <label for="">Select Start Date</label>
                            <div class="position-relative has-icon-left">
                                <input placeholder="From Date" class="form-control "  name="fromdate" type="date"  value="<?php echo $this->input->post('fromdate') ?>" id="fromDate"> 
                              <div class="form-control-position">
                                <i class="ft-calendar"></i>
                              </div>
                            </div>
                          </div>
                          <div class="form-group col-md-3 mb-2">
                            <label for="">Select End Date</label>
                            <div class="position-relative has-icon-left">
                               <input placeholder="dd-mm-yyyy"  class="form-control datepicker" id="toDate" name="todate" type="date"   value="<?php echo $this->input->post('todate') ?>" > 
                              <div class="form-control-position">
                                <i class="ft-calendar"></i>
                              </div>
                            </div>
                          </div>
                            <div class="form-group col-md-3 mb-2">
                            <label for="">-</label>
                            <div class="position-relative has-icon-left">
                              <button type="submit" name="submit" id=""  class="btn  btn-success">Search</button>  
                              
                            </div>
                          </div>
                        </div>
                         <?php echo form_close(); ?>
                      </div>
                      
                      
                    <table id="" class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th hidden="">#Id</th>
                          <th> संदर्भ क्रमांक </th>
                          <th> बारकोड क्रमाँक </th>
                          <th> कार्डधारक नाव </th>
                          <th> मोबाइल नंबर </th>
                          <th> कार्ड प्रकार </th>
                          <th>Visit date</th>
                        </tr>
                      </thead>
                      <tbody id="">
                            <?php foreach ($lists as $lists) { ?>
                          <tr style="font-size:13px;" id="autoSuggestionsList">
                            <td style="width: 2%" hidden="">1 </span></td>
                            <td> <span class="badge badge-pill badge-primary">
                                <?php echo $lists->rel_id ?> </span>
                            </td>
                            <td> <?php echo $lists->barcode_no; ?> </td>
                            <td><?php echo $lists->c_name; ?></td>
                            <td><?php echo $lists->c_mobile; ?></td>
                         
                            <td><?php echo $lists->c_cardtype; ?></td>
                            <td>
                                <?php  $dateform =  $lists->entry_date;
                                       $newDate = date("F j, Y", strtotime($dateform));
                                ?>
                                <span class="badge badge-pill badge-primary"><?php echo $newDate;?></span>
                            </td>
                        </tr>
                          <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    </div>
</div>
<?php require 'footer.php';?>