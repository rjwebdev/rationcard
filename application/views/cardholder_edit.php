<?php require 'header.php';?>

<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0">Ration Card Holder</h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>cardholder"> Card Holder</a>
                </li>
                <li class="breadcrumb-item"><a href="#">New</a>
                </li>
                <li class="breadcrumb-item active">Update Cardholder
                </li>
              </ol>
            </div>
          </div>
        </div>
        
      </div>
        <!--//card layout-->
        <div class="content-body">

        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                  <div class="card-header bg-panel" style="">
                    <h4 class="card-title">
                        <i class="la la-user-plus" ></i> || Update cardholder
                    </h4>
                 </div>
                <div class="card-content collapse show">
                  <div class="card-body ">

                     <?php echo form_open("cardholder/update_cardholder/$cardholder->c_id");?>
                       <?php echo form_hidden('update_on',  date('Y-m-d H:i:s')); ?>
                      <div class="form-body">
                          <div class="col-md-9 offset-md-1">
                        <div class="row">
                          <div class="form-group col-md-6 mb-2">
                             <label for="">Reference No. ( संदर्भ क्रमांक )</label>
                            <div class="position-relative has-icon-left">
                               <?php echo form_input(array('name'=>'rel_id','class'=>'form-control border-success','placeholder'=>'संदर्भ क्रमांक','value'=>set_value('rel_id',$cardholder->rel_id))) ?>
                                <div class="form-control-position">
                                <i class="la la-recycle"></i>
                              </div>
                            </div>
                          </div>
                          <div class="form-group col-md-6 mb-2">
                            <label for="">Barcode No. ( बारकोड क्रमाँक )</label>
                            <div class="position-relative has-icon-left">
                               <?php echo form_input(array('name'=>'barcode_no','class'=>'form-control border-success','placeholder'=>'बारकोड क्रमाँक','value'=>set_value('barcode_no',$cardholder->barcode_no))) ?>
                                <div class="form-control-position">
                               <i class="la la-barcode"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6 mb-2">
                            <label for="">Cardholder Name ( कार्डधारक नाव )</label>
                            <div class="position-relative has-icon-left">
                                <?php echo form_input(array('name'=>'c_name','class'=>'form-control border-success','placeholder'=>'कार्डधारक नाव','value'=>set_value('c_name',$cardholder->c_name))) ?>
                              <div class="form-control-position">
                               <i class="la la-user"></i>
                              </div>
                            </div>
                          </div>
                          <div class="form-group col-md-6 mb-2">
                            <label for=""> Mobile No. ( मोबाइल नंबर )</label>
                            <div class="position-relative has-icon-left">
                              <?php echo form_input(array('name'=>'c_mobile','class'=>'form-control border-success','placeholder'=>'मोबाइल नंबर','value'=>set_value('c_mobile',$cardholder->c_mobile))) ?>
                              <div class="form-control-position">
                               <i class="la la-tty"></i>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="row">
<!--                          <div class="form-group col-6 mb-2">
                            <label for="">Unit ( युनिट )</label>
                             <div class="position-relative has-icon-left">
                                 <?php echo form_input(array('name'=>'c_unit','class'=>'form-control border-success','placeholder'=>'युनिट','value'=>set_value('c_unit',$cardholder->c_unit))) ?>
                            <div class="form-control-position">
                            <i class="la la-sitemap"></i>
                              </div>
                            </div>
                          </div>-->
                          <div class="form-group col-2  mb-2">
                            <label> Unit(युनिट)</label>
                            <div class="position-relative has-icon-left">
                                <input type="text" name="" id="input1" value="2" onkeyup="calc()" hidden=""  >
                                <?php echo form_input(array('name'=>'c_unit','id'=>'input2','onkeyup'=>'calc()','class'=>'form-control border-success',''=>'','placeholder'=>'युनिट','value'=>set_value('adult',$cardholder->adult /2))) ?>
                                <!--<input type="text" class="form-control " placeholder="adult" onkeyup="calc()"  value="" name="" id="input2" style="">-->
                                <div class="form-control-position">
                                  <i class="la la-mars-stroke"></i>
                              </div>  
                              </div>
                           </div>
                        <div class="form-group col-2 mb-2">
                            <label for="">-</label>
                             <?php echo form_input(array('name'=>'adult','id'=>'output','class'=>'form-control border-success','readonly'=>'','placeholder'=>'Adults','value'=>set_value('adult',$cardholder->adult))) ?>
                            <!--<input type="text" name="adult" id="output" value="" class="form-control" readonly="" >-->
                          </div>
                            <div class="form-group col-2 mb-2">
                            <label for="">-</label>
                             <div class="position-relative has-icon-left">
                                  <?php echo form_input(array('name'=>'child','id'=>'','class'=>'form-control border-success',''=>'','placeholder'=>'Childs','value'=>set_value('adult',$cardholder->child))) ?>
                                 <!--<input type="text" id="" class="form-control" required="" placeholder="child" name="child">-->
                            <div class="form-control-position">
                              <i class="la la-child"></i>
                              </div>
                            </div>
                          </div>

                            <div class="form-group col-6 mBudgetb-2">
                            <label for="">Card Type ( कार्ड प्रकार )</label>
                             <div class="position-relative has-icon-left">
                                 <select id=""  class="form-control border-success" name="c_cardtype">
                                  <option value="<?php echo $cardholder->c_cardtype ?>" class="" ><?php echo $cardholder->c_cardtype ?></option>
                                  
                                 <option value="anna suraksha"> Anna Suraksha </option>
                                  <option value="apl"> APL </option>
                                   <option value="NG"> NG </option>
                                   <option value="anntodhay"> Anntodhay </option>
                            </select>
                            <div class="form-control-position">
                           <i class="la la-credit-card"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-6 mBudgetb-2">
                            <label for="">Bank Name</label>
                            <div class="" id="banknameO">
                                <select  class="form-control show-tick border-success" name="bnk_name" id="bnk_name" data-live-search="true" style="border-bottom: 1px solid #ccc;" >
                                       <option value="<?php echo $cardholder->bnk_name ?>" class="" ><?php echo $cardholder->bnk_name ?></option>
                                        <?php foreach($bankname as $key => $bankname) {?>
                                        <option value="<?php echo $bankname->bnk_name; ?>"><?php echo $bankname->bnk_name; ?></option>";
                                        <?php } ?>  
                                </select>
                              </div>
                          </div>
                            <div class="form-group col-6 mb-2">
                            <label for="">Bank Account No</label>
                            <div class="position-relative has-icon-left">
                                <?php echo form_input(array('name'=>'bnk_accno','id'=>'','class'=>'form-control border-success',''=>'','placeholder'=>'Bank Account no','value'=>set_value('bnk_accno',$cardholder->bnk_accno))) ?>
                              <div class="form-control-position">
                               <i class="la la-tty"></i>
                              </div>
                            </div>
                          </div>
                        </div>
<!--                <div class="repeater-default">
                      <div data-repeater-list="row" >
                        <div data-repeater-item>-->
                            <?php if(count($cardholder_details) ): 
                                     $count=1;
			          foreach($cardholder_details as $cardholder_details):?> 
<!--<div class="repeater-default">-->
<!--                 <div data-repeater-list="row" >
                        <div data-repeater-item>-->
                         <div class="row " >
                             <div class="form-group col-md-1 mb-2">
                            <label for="">-</label>
                            <div class="position-relative has-icon-left">
                              <div class="form-control-position">
                                  <input type="hidden" name="cd_id[]" id="cd_id[]" value="<?php echo $cardholder_details->cd_id;?>">
                                  <i class="la la-user" style="color:#666ee8"></i> <span style="color:#000"><?php echo $count++  ?></span>
                              </div>
                            </div>
                          </div>
                          <div class="form-group col-md-6 mb-2">
                            <label for="">Cardholder Family Name ( कार्डधारक नाव )</label>
                            <div class="position-relative has-icon-left">
                                <?php echo form_input(array('name'=>'cm_name[]','id'=>'cm_name[]','class'=>'form-control border-primary',''=>'','placeholder'=>'Cardholder family Name','value'=>set_value('cm_name',$cardholder_details->cm_name))) ?>
                              <div class="form-control-position">
                               <i class="la la-user"></i>
                              </div>
                            </div>
                          </div>
                          <div class="form-group col-md-5 mb-2">
                            <label for=""> Mobile No. ( मोबाइल नंबर )</label>
                            <div class="position-relative has-icon-left">
                                <?php echo form_input(array('name'=>'cm_mobile[]','id'=>'cm_mobile[]','class'=>'form-control border-primary',''=>'','placeholder'=>'मोबाइल नंबर','value'=>set_value('cm_mobile',$cardholder_details->cm_mobile))) ?>
                              <div class="form-control-position">
                               <i class="la la-tty"></i>
                              </div>
                            </div>
                          </div>
                        </div>
<!--                        </div>
                 </div>
</div>-->
                        <?php endforeach; ?>
	                     <?php else: ?>
	                <?php endif; ?>
<!--                       </div></div>
                         </div>-->
                    </div>
                              
                          </div>
                        <!--//footer page-->
                        <div class="col-md-9 offset-1 ">   
                        <?php 
				         echo form_reset(array('name'=>'reset','value'=>'RESET','class'=>'btn btn-warning mr-1','type'=>'submit')),
					form_submit(array('name'=>'submit','value'=>'UPDATE CARDHOLDER','class'=>'btn btn-primary','onclick'=>'return updateemployee();'));
			              ?>
                            
                      </div>
                            <?php form_close(); ?>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
       <!--//end card body-->
        </div>
      <!--//end content wrapper-->  
    </div>
</div>

 <script>
function calc() {
    var textValue1 = document.getElementById('input1').value;
    var textValue2 = document.getElementById('input2').value;

    document.getElementById('output').value = textValue1 * textValue2;
}
</script>
<script>
    
(function(window, document, $) {
	'use strict';

	// Default
	$('.repeater-default').repeater();

	// Custom Show / Hide Configurations
	$('.file-repeater, .contact-repeater').repeater({
		show: function () {
			$(this).slideDown();
		},
		hide: function(remove) {
			if (confirm('Are you sure you want to remove this item?')) {
				$(this).slideUp(remove);
			}
		}
	});


})(window, document, jQuery);
    </script>
<?php require 'footer.php';?>
