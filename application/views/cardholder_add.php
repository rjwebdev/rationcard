<?php require 'header.php';?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0">Ration Card Holder</h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>cardholder"> Card Holder</a>
                </li>
                <li class="breadcrumb-item"><a href="#">New</a>
                </li>
                <li class="breadcrumb-item active">Add Cardholder
                </li>
              </ol>
            </div>
          </div>
        </div>
        
      </div>
        <!--//card layout-->
        <div class="content-body">

        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                  <div class="card-header bg-panel" style="">
                    <h4 class="card-title">
                        <i class="la la-user-plus" ></i> || Add New cardholder
                    </h4>
                 </div>
                <div class="card-content collapse show">
                  <div class="card-body ">
                      

                      
                    <?php $attributes = array('id' => 'cardholderform'); echo form_open_multipart('cardholder/store_cardholder',$attributes);?>
                            <?php echo form_hidden('ccreate_at',  date('Y-m-d H:i:s')); ?>
                          
                       <input type="hidden" name="id" value="<?php echo $_SESSION['id']; ?>">
                      <div class="form-body">
                          <div class="col-md-9 offset-md-1">
                        <div class="row">
                          <div class="form-group col-md-6 mb-2">
                             <label for="">Reference No. ( संदर्भ क्रमांक )</label>
                            <div class="position-relative has-icon-left">
                                <input type="text" id="" class="form-control" required="" placeholder="संदर्भ क्रमांक"  name="rel_id">
                              <div class="form-control-position">
                                <i class="la la-recycle"></i>
                              </div>
                            </div>
                          </div>
                          <div class="form-group col-md-6 mb-2">
                            <label for="">Barcode No. ( बारकोड क्रमाँक )</label>
                            <div class="position-relative has-icon-left">
                                <input type="text" id="" class="form-control"  required="" placeholder="बारकोड क्रमाँक"  name="barcode_no">
                              <div class="form-control-position">
                               <i class="la la-barcode"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6 mb-2">
                            <label for="">Cardholder Name ( कुटुंब प्रमुखाच नाव )</label>
                            <div class="position-relative has-icon-left">
                                <input type="text" id="" class="form-control" required="" placeholder="कार्डधारक नाव"  name="c_name">
                              <div class="form-control-position">
                               <i class="la la-user"></i>
                              </div>
                            </div>
                          </div>
                          <div class="form-group col-md-6 mb-2">
                            <label for=""> Mobile No. ( मोबाइल नंबर )</label>
                            <div class="position-relative has-icon-left">
                              <input type="text" id="" class="form-control" placeholder="मोबाइल नंबर"  name="c_mobile">
                              <div class="form-control-position">
                               <i class="la la-tty"></i>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="form-group col-2  mb-2">
                            <label> Unit(युनिट)</label>
                            <div class="position-relative has-icon-left">
                                <input type="text" name="" id="input1" value="2" onkeyup="calc()" hidden=""  >
                                <input type="text" class="form-control " placeholder="adult" onkeyup="calc()"  value="" name="" id="input2" style="">
                                <div class="form-control-position">
                                  <i class="la la-mars-stroke"></i>
                              </div>  
                              </div>
                            </div>
                      
                            <div class="form-group col-2 mb-2">
                            <label for="">-</label>
                            <input type="text" name="adult" id="output" value="" class="form-control" readonly="" >
                          </div>
                            <div class="form-group col-2 mb-2">
                            <label for="">-</label>
                             <div class="position-relative has-icon-left">
                                 <input type="text" id="" class="form-control" required="" placeholder="child" name="child">
                            <div class="form-control-position">
                              <i class="la la-child"></i>
                              </div>
                            </div>
                          </div>
                            <div class="form-group col-6 mBudgetb-2">
                            <label for="">Card Type ( कार्ड प्रकार )</label>
                             <div class="position-relative has-icon-left">
                                 <select id=""  class="form-control" name="c_cardtype">
                                  <option value="">Select Card Type</option>
                                  <option value="anna suraksha"> Anna Suraksha </option>
                                  <option value="apl"> APL </option>
                                   <option value="NG"> NG </option>
                                   <option value="anntodhay"> Anntodhay </option>
                            </select>
                            <div class="form-control-position">
                           <i class="la la-credit-card"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                              
                           <!--//backdetails-->
                        <div class="row">
                          <div class="col-md-6 mb-2">
                              <div class="form-group ">
                                  
                            <label for="">Bank Name</label>
                            <div class="" id="banknameO">
                                <select  class="form-control show-tick" name="bnk_name" id="bnk_name" data-live-search="true" style="border-bottom: 1px solid #ccc;" >
                                    <option value="">--- Select Bank Name ---</option>
                                       <option value="New" class="bg-primary font-bold"  > Create New Bank </option>
                                        <?php foreach($bankname as $key => $bankname) {?>
                                        <option value="<?php echo $bankname->bnk_name; ?>"><?php echo $bankname->bnk_name; ?></option>";
                                        <?php } ?>  
                                </select>
                            </div>
                            
                            <!--//create bank code-->
                            <!--<div class="col-md-6">-->
                             <div id="chnageInput1" class="col-md-12">
                                        <div class="form-group form-float">
                                    <div class="position-relative has-icon-left">
                                        <input name="banknameN"  type="text"  id="banknameN"  placeholder="Create New Bank Name" class="form-control" />
                                        <div class="form-control-position">
                                    <i class="la la-bank"></i>
                              </div>
                                    </div>
                                     <span id="bnk_name_result"></span>  
                                </div>
                               </div>
                            
                              </div>
                          </div>
                          <div class="form-group col-md-6 mb-2">
                            <label for="">Bank Account No</label>
                            <div class="position-relative has-icon-left">
                              <input type="text" id="" class="form-control" placeholder="Account No"  name="bnk_accno">
                              <div class="form-control-position">
                               <i class="la la-tty"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                              
                              
                              
                              
                              
                              <!--\\card repertr-->
                    <div class="repeater-default">
                      <div data-repeater-list="row" >
                        <div data-repeater-item>
                              
                         <div class="row "  >
                          <div class="form-group col-md-6 mb-2">
                            <label for="">Cardholder Family Name ( कार्डधारक नाव )</label>
                            <div class="position-relative has-icon-left">
                                <input type="text" id="cm_name[]" class="form-control" required="" placeholder="कार्डधारक नाव"  name="cm_name">
                              <div class="form-control-position">
                               <i class="la la-user"></i>
                              </div>
                            </div>
                          </div>
                          <div class="form-group col-md-4 mb-2">
                            <label for=""> Mobile No. ( मोबाइल नंबर )</label>
                            <div class="position-relative has-icon-left">
                                <input type="text"  id="cm_mobile[]" class="form-control" placeholder="मोबाइल नंबर"  name="cm_mobile">
                              <div class="form-control-position">
                               <i class="la la-tty"></i>
                              </div>
                            </div>
                          </div>
                                <div class="form-group col-sm-12 col-md-2 text-center mt-2">
                              <button type="button" class="btn btn-danger" data-repeater-delete> <i class="ft-x"></i> Delete</button>
                            </div>
                        </div>
                        </div>
                      </div>
                        <!--//add button-->
                        <div class="form-group overflow-hidden">
                        <div class="col-12">
                          <button data-repeater-create class="btn btn-primary">
                            <i class="ft-plus"></i> Add
                          </button>
                        </div>
                      </div>
                    </div>
                          
                          </div>
                        <!--//footer page-->
                        <div class="col-md-9 offset-1 ">   
                         <?php 
			      echo form_reset(array('name'=>'reset','value'=>'RESET','class'=>'btn btn-warning mr-1')),
			           form_submit(array('name'=>'submit','value'=>'ADD CARDHOLDER','class'=>'btn btn-primary','onclick'=>' return addemployee();'));
			    ?>
                            
                      </div>
                            <?php form_close(); ?>

                    <!--//card boy close-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
            
       <!--//end card body-->
        </div>
      <!--//end content wrapper-->  
    </div>
</div>
 <script>
function calc() {
    var textValue1 = document.getElementById('input1').value;
    var textValue2 = document.getElementById('input2').value;

    document.getElementById('output').value = textValue1 * textValue2;
}
</script>
<script>
    $(document).ready(function(e) {
     
        $("#chnageInput1").css("display", 'none');

         $("#bnk_name").change(function(e) {
            var type = $(this).val();
            if (type == "New") {
                $("#chnageInput1").css("display", 'inline-block');
                $("#banknameO").attr("class", "col-md-12")
            } else {
                $("#chnageInput1").css("display", 'none');
                $("#banknameO").removeClass("col-md-12")
            }
        });
    });
</script>
<script>
    
(function(window, document, $) {
	'use strict';

	// Default
	$('.repeater-default').repeater();

	// Custom Show / Hide Configurations
	$('.file-repeater, .contact-repeater').repeater({
		show: function () {
			$(this).slideDown();
		},
		hide: function(remove) {
			if (confirm('Are you sure you want to remove this item?')) {
				$(this).slideUp(remove);
			}
		}
	});


})(window, document, jQuery);
    </script>

    <script>
    $(document).ready(function(){  
      $('#banknameN').change(function(){  
           var bnk_name = $('#banknameN').val();  
           alert(bnk_name);
           if(bnk_name != '')  
           {  
                $.ajax({  
                     url:"<?php echo base_url(); ?>cardholder/check_bnk_name_avalibility",  
                     method:"POST",  
                     data:{bnk_name:bnk_name},  
                     success:function(data){  
//                         alert(data);
                          $('#bnk_name_result').html(data);  
                     }  
                });  
           }  
      }); 
      });
      </script>
<?php require 'footer.php';?>