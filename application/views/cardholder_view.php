<?php require 'header.php';?>

<div class="app-content content">
    <div class="content-wrapper">
        
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0">Contacts</h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href=""> Cardholder </a>
                </li>
                <li class="breadcrumb-item active">Ration card profile
                </li>
              </ol>
            </div>
          </div>
        </div>
      </div>
       
     
        
      <div class="content-detached content-right">
        <div class="content-body">
          <section class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-head">
                  <div class="card-header bg-panel">
                      <h4 class="card-title"> <i class="fa fa-user"></i> || All Card Info</h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                    <div class="heading-elements">
                      
<!--                      <span class="dropdown">
                        <button id="btnSearchDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="true" class="btn btn-warning dropdown-toggle dropdown-menu-right btn-sm"><i class="ft-download-cloud white"></i></button>
                        <span aria-labelledby="btnSearchDrop1" class="dropdown-menu mt-1 dropdown-menu-right">
                          <a href="#" class="dropdown-item"><i class="ft-upload"></i> Import</a>
                          <a href="#" class="dropdown-item"><i class="ft-download"></i> Export</a>
                          <a href="#" class="dropdown-item"><i class="ft-shuffle"></i> Find Duplicate</a>
                        </span>
                      </span>-->
                        
                        <a class="btn btn-default btn-sm" href="javascript:history.go(-1);"><i class="fa fa-times fa-2x"></i></a>
                      
                    </div>
                  </div>
                </div>
                <div class="card-content">
                  <div class="card-body">

                   <ul class="nav nav-pills nav-justified">
                      <li class="nav-item">
                          <a class="nav-link active" id="homeEle2-tab" data-toggle="pill" href="#homeEle2" aria-expanded="true" style="text-transform:capitalize;">
                            <i class="la la-home"></i> Cardholder Family
                        </a>
                      </li>
<!--                      <li class="nav-item">
                        <a class="nav-link " id="profileEle2-tab" data-toggle="pill" href="#profileEle2"
                        aria-expanded="false"><i class="la la-user"></i></a>
                      </li>-->
                      <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                        aria-haspopup="true" aria-expanded="false">
                         -
                        </a>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" id="dropdownEle21-tab" href="#dropdownEle21" data-toggle="pill"
                          aria-expanded="true"><i class="la la-fighter-jet"></i> -</a>
                          <a class="dropdown-item" id="dropdownEle22-tab" href="#dropdownEle22" data-toggle="pill"
                          aria-expanded="true"><i class="la la-fighter-jet"></i> _</a>
                        </div>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="aboutEle2-tab" data-toggle="pill" href="#aboutEle2" aria-expanded="false">-</a>
                      </li>
                    </ul>
                      
                      <div class="tab-content" >
                          
                      <div role="tabpanel" class="tab-pane active show" id="homeEle2" aria-labelledby="homeEle2-tab" aria-expanded="false">
                        <p>----------------------</p>
                        <div class="table-responsive">
                            <table id="" class="table table-sm mb-0">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Age</th>
                          </tr>
                        </thead>
                        <tbody>
                             <?php if(count($cardholder_details) ): 
                                              $count=1;
			                      foreach($cardholder_details as $cardholder_details):?>
                          <tr>
                            <td>
                              <?= $count++ ?>
                            </td>
                            <td>
                              <div class="media">
                                <div class="media-left pr-1">
                                  <span class="avatar avatar-sm avatar-online rounded-circle">
                                      <img src="<?php echo base_url(); ?>assets/img/user.png" alt=""><i></i></span>
                                </div>
                                <div class="media-body media-middle">
                                  <a href="#" class="media-heading"><?php echo $cardholder_details->cm_name ?></a>
                                </div>
                              </div>
                            </td>
                            <td><?php echo $cardholder_details->cm_mobile; ?></td>
                            <td class="text-center">-</td>

                          </tr>
                          <?php endforeach; ?>
	                     <?php else: ?>
	                <?php endif; ?>
                        </tbody>
                        
                      </table>
                        </div>
                      </div>
                      <div class="tab-pane " id="profileEle2" role="tabpanel" aria-labelledby="profileEle2-tab"  aria-expanded="true">
                        <p>Pudding candy canes sugar plum cookie chocolate cake powder</p>
                        
                      </div>
                      <div class="tab-pane" id="dropdownEle21" role="tabpanel" aria-labelledby="dropdownEle21-tab" aria-expanded="false">
                        <p>Cake croissant lemon drops gummi bears carrot cake biscuit
                          cupcake croissant. Macaroon lemon drops muffin jelly sugar
                          plum chocolate cupcake danish icing. Soufflé tootsie roll
                          lemon drops sweet roll cake icing cookie halvah cupcake.</p>
                      </div>
                      <div class="tab-pane" id="dropdownEle22" role="tabpanel" aria-labelledby="dropdownEle22-tab" aria-expanded="false">
                        <p>Chocolate croissant cupcake croissant jelly donut. Cheesecake
                          toffee apple pie chocolate bar biscuit tart croissant.
                          Lemon drops danish cookie. Oat cake macaroon icing tart
                          lollipop cookie sweet bear claw.</p>
                      </div>
                      <div class="tab-pane" id="aboutEle2" role="tabpanel" aria-labelledby="aboutEle2-tab" aria-expanded="false">
                        <p>Carrot cake dragée chocolate. Lemon drops ice cream wafer
                          gummies dragée. Chocolate bar liquorice cheesecake cookie
                          chupa chups marshmallow oat cake biscuit. Dessert toffee
                          fruitcake ice cream powder tootsie roll cake.</p>
                      </div>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </section>
            <!--//new-->
            
        </div>
      </div>
        
    <?php foreach ($cardholder_data as $cardholder_data): ?>
        
      <div class="sidebar-detached sidebar-left">
        <div class="sidebar">
          <div class="bug-list-sidebar-content">
            <!-- Predefined Views -->
            <div class="card">
              <div class="card-head">
                <div class="media p-1">
                  <div class="media-left pr-1">
                    <span class="avatar avatar-sm ">
                      <img src="<?php echo base_url(); ?>assets/img/user.png" alt="avatar"><i></i></span>
                  </div>
                  <div class="media-body media-middle">
                    <h5 class="media-heading"><?php echo $cardholder_data->c_name; ?></h5>
                    <!--<span style="font-size:12px;color:#666ee8">Cardholder Name(कुटुंब प्रमुखाच नाव)</span>-->
                  </div>
                </div>
              </div>
             
              <div class="card-body">
                <p class="lead">Info card </p>
                <ul class="list-group" style="font-size:13px;">
                  <li class="list-group-item">
                      <span class="badge  float-right" style="color:#666666;text-transform: uppercase"><?php echo $cardholder_data->rel_id; ?></span>
                      <i class="fa fa-recycle"></i> Ref.No. 
                  </li>
                  <li class="list-group-item">
                    <span class="badge bg-primary float-right" style="color:#fff;font-weight: bold;text-transform: uppercase"><?php echo $cardholder_data->barcode_no; ?></span>
                    <i class="fa fa-barcode"></i> Barcode No.
                  </li>
                  <li class="list-group-item">
                    <span class="badge bg-primary float-right" style="color:#fff;font-weight: bold;text-transform: uppercase"><?php echo $cardholder_data->c_mobile; ?></span>
                    <i class="fa fa-mobile-phone"></i>   Mobile No. 
                  </li>
                  <li class="list-group-item">
                    <span class="badge bg-primary float-right" style="color:#fff;font-weight: bold;text-transform: uppercase"><?php echo $cardholder_data->child; ?></span>
                    <i class="fa fa-user"></i>  UNIT:   Adult ---
                   <span class="badge bg-primary float-right" style="color:#fff;font-weight: bold;text-transform: uppercase"><?php echo $cardholder_data->adult; ?></span>
                   Child
                  </li>
                  <li class="list-group-item">
                    <span class="badge bg-primary float-right" style="color:#fff;font-weight: bold;text-transform: uppercase"><?php echo $cardholder_data->c_cardtype; ?></span>
                    <i class="fa fa-cart-plus"></i> Card Type
                  </li>
                  <li class="list-group-item">
                    <span class="badge bg-primary float-right" style="color:#fff;font-weight:bold;text-transform: uppercase"><?php echo $cardholder_data->bnk_name; ?></span>
                    <i class="fa fa-bank"></i> Bank Na.
                  </li>
                  <li class="list-group-item">
                    <span class="badge bg-info  float-right" style="color:#fff;font-weight: bold;text-transform: uppercase"><?php echo $cardholder_data->bnk_accno; ?></span>
                    <i class="fa fa-cart-plus"></i>  Bank Acc
                  </li>
                </ul>
              </div>

            </div>
            <!--/ Predefined Views -->
          </div>
        </div>
      </div>
         <?php endforeach; ?>  
    </div>
  </div>


<?php require 'footer.php';?>