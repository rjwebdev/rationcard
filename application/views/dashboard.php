<?php require 'header.php';?>

  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
          
        <div class="row">
          <div class="col-xl-6 col-12">
            <div class="card">
                <div class="card-header ">
                <h4 class="card-title">@ Graphs @</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-content collapse show">
                <div class="card-body pt-0">
                  <div class="row mb-1">
                    <div class="">
                       <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Blue/ Pink/ Red',     11],
          ['Orange',      2],
          ['Yellow ',    7]
        ]);
        var options = {
          title: ''
        };
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
                    <div id="piechart" style="width:500px ; height:250px;"></div>

                  </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-6 col-12">
            <div class="row">
              <div class="col-lg-6 col-12">
                <div class="card pull-up">
                  <div class="card-content">
                    <div class="card-body">
                      <div class="media d-flex">
                        <div class="media-body text-left">
                          <h6 class="text-muted">Total Cardholder </h6>
                          <h3 class=""> <?php echo $carholder_count; ?> </h3>
                        </div>
                        <div class="align-self-center">
                            <span class="icon-profile" style="font-size:40px;">
                                <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span>
                            </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6 col-12">
                <div class="card pull-up">
                  <div class="card-content">
                    <div class="card-body">
                      <div class="media d-flex">
                        <div class="media-body text-left">
                          <h6 class="text-muted">Total Family Members</h6>
                          <h3><?php echo $total= $carholder_familymembers +  $carholder_count; ?></h3>
                        </div>
                        <div class="align-self-center">
                                <span class="icon-presentation" style="font-size:40px;">
                                    <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span>
                            </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
              <div class="row">
              <div class="col-lg-6 col-12">
                <div class="card pull-up">
                  <div class="card-content">
                    <div class="card-body">
                      <div class="media d-flex">
                        <div class="media-body text-left">
                          <h6 class="text-muted">Monthly Visitors </h6>
                          <h3><?php echo date("F"); ?>  <?php echo  $monthly_visiter; ?> </h3>
                        </div>
                        <div class="align-self-center">
                            <span class="icon-idea" style="font-size:40px;">
                                <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span>
                            </span>       
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6 col-12">
                <div class="card pull-up">
                  <div class="card-content">
                    <div class="card-body">
                      <div class="media d-flex">
                        <div class="media-body text-left">
                          <h6 class="text-muted">Mobiles No</h6>
                          <h3><?php echo $mobile_no; ?></h3>
                        </div>
                        <div class="align-self-center">
                            <span class="icon-rocket-launch" style="font-size: 40px;">
                                <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span>
                            </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
              <div class="row">
              <div class="col-lg-6 col-12">
                <div class="card pull-up">
                  <div class="card-content">
                    <div class="card-body">
                      <div class="media d-flex">
                        <div class="media-body text-left">
                          <h6 class="text-muted">Total Users </h6>
                          <h3><?php echo $user_count; ?></h3>
                        </div>
                        <div class="align-self-center">
                            <span class="icon-working-hours" style="font-size:40px;">
                         <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6 col-12">
                <div class="card pull-up">
                  <div class="card-content">
                    <div class="card-body">
                      <div class="media d-flex">
                        <div class="media-body text-left">
                          <h6 class="text-muted">Types of card</h6>
                          <h3>4</h3>
                        </div>
                        <div class="align-self-center">
                            <span class="icon-laptop" style="font-size: 40px;">
                             <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span><span class="path32"></span><span class="path33"></span><span class="path34"></span><span class="path35"></span><span class="path36"></span><span class="path37"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        
        <div class="row">
          
          <div id="recent-sales" class="col-12 col-md-12">
            <div class="card">
                <div class="card-header bg-panel" style="">
                    <h4 class="card-title">
                          <i class="fa fa-user fa-fw"></i> Recent Records</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li>
                        <a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="<?php echo base_url(); ?>rationcardholder" >View all</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="card-content mt-1">
                <div class="table-responsive">
                  <table id="recent-orders" class="table table-hover table-xl mb-0">
                    <thead>
                      <tr>
                            <th>#id</th>
                          <th> संदर्भ क्रमांक </th>
                          <th> बारकोड क्रमाँक </th>
                          <th> कार्डधारक नाव </th>
                          <th> मोबाइल नंबर </th>
                         
                          <th> कार्ड प्रकार </th>
                          <th> तारीख </th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php if(count($recent_data) ): 
                              $count=1;
			     foreach($recent_data as $recent_data):?>
                      <tr>
                            <td style="width: 2%" ><?php echo $count++; ?> </td>
                            <td> <span class="badge badge-pill badge-danger"><?php echo $recent_data->rel_id ?> </span> </td>
                            <td> <?php echo $recent_data->barcode_no; ?> </td>
                            <td><?php echo $recent_data->c_name; ?></td>
                            <td><?php echo $recent_data->c_mobile; ?></td>
                           
                            <td><?php echo $recent_data->c_cardtype; ?></td>
                            <td>
                                <?php  $dateform = $recent_data->ccreate_at;
                                       $newDate = date("F j, Y", strtotime($dateform));
                                ?>
                                <span class="badge badge-pill badge-primary"><?php echo $newDate;?></span>
                               
                            </td>
                        </tr>
                      <?php endforeach; ?>
                        <?php else: ?>
                        <?php endif; ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
       
        

      </div>
    </div>
  </div>
  

<?php require 'footer.php';?>
