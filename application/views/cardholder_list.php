<?php require 'header.php'; ?>
<script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
<script>
    window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 4000);
</script>
<style>
    .btn-mda{
            line-height: 0;
            margin-top: -4px;
            border-top-right-radius: 10px;
           border-bottom-right-radius: 10px;
            /*padding-top: 3px;*/
    }
    </style>
    <style>
.onoffswitch {
    position: relative; 
    width: 100px;
    -webkit-user-select:none;
    -moz-user-select:none;
    -ms-user-select: none;
     box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
} 
.onoffswitch-checkbox {
    display: none;
}
.onoffswitch-label {
    display: block; 
    overflow: hidden;
    cursor: pointer;
} 
.onoffswitch-inner {
    display: block;
    width: 200%;
    margin-left: -100%; 
    transition: margin 0.3s ease-in 0s;
}
.onoffswitch-inner:before, .onoffswitch-inner:after { 
    display: block; 
    float: left; 
    width: 50%;
    height: 30px;
    padding: 0; 
    line-height: 26px;
    font-size: 14px;
    color: white;
    font-weight: bold; box-sizing: border-box;
} 
.onoffswitch-inner:before { 
    content: "No";
    padding-left: 10px;
    background-color: #327D1F;
    color: #FFFFFF;
    border: 1px solid #f44336;
}
.onoffswitch-inner:after { 
    content: "Yes";
    padding-right: 10px;
    padding-bottom: 5px;
    padding-left: 10px;
    background-color: #f44336;
    color: #FFFFFF;
} 
.onoffswitch-switch {
    display: block;
    width: 18px;
    margin: 0px;
    position: absolute;
    top: 0; bottom: 0; 
    right: 77px; 
    transition: all 0.3s ease-in 0s;
}
.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner { 
    margin-left: 0;
}
.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch { 
    right: 0px; }
   .delete{
            background: #fff;
            border: none;
            padding-right: 60px;
        }
        .delete:hover {
            background: #e9e9e9;
            
        }
</style>
<script type="text/javascript">
            function ajaxSearch(){
                var input_data = $('#search_data').val();
//                alert(input_data);
                if (input_data.length === 0){
                    $('#suggestions').hide();
                }else{
                    var post_data = {
                        'search_data': input_data
                    };
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>cardholder/autocomplete/",
                        data: post_data,
                        success: function (data) {
                            alert(data);
                            // return success
                            if (data.length > 0) {
                                $('#suggestions').show();
                                $('#autoSuggestionsList').addClass('auto_list');
                                $('#autoSuggestionsList').html(data);
                            }
                        }
                    });
                }
            }
        </script>
      <script>
function myFunction() {
    location.reload();
}
</script>  
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0"> Cardholder </h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>cardholder"> Cardholder </a>
                </li>
                <li class="breadcrumb-item"><a href="#"> Active </a>
                </li>
                <li class="breadcrumb-item active">List Cardholders
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-header-right text-md-right col-md-6 col-12">
          <div class="btn-group" >
              
               
          </div>
        </div>
      </div>
        <!--//card layout-->
        <div class="content-body">

        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                  <div class="card-header bg-panel" style="">
                    <h4 class="card-title">
                        <i class="la la-cc-diners-club" ></i> || All Active Cardholders List</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                     
                      <li>
                        
                          <div class="btn-group" role="group" aria-label="">
                            <a href="<?php echo base_url(); ?>cardholder/add_cardholder"  onclick="return addcardholder();" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Add New Cardholder"> <i class="la la-plus-circle"></i> Add </a>
                            <!--<a  data-toggle="modal" href="#myModal" onclick="" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Import Excel Data"> <i class="la la-file-excel-o"></i> Import </a>-->
                            <a href="<?php echo base_url(); ?>cardholder/export"  onclick="" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Export Data"> <i class="la la-list-alt"></i> Export </a>
                            <a data-action="reload"  onclick="myFunction()" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Refresh Data" style="color:#fff;"> <i class="ft-rotate-cw"></i> Refresh </a>
                          </div>
                        
                      </li>
                    </ul>
                  </div>
                </div>
                  <!--//import excel-->
                  
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
           <h4 class="modal-title" id="myModalLabel">Import Data Via Excel</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
       
      </div>
      <div class="modal-body">
        <form method="post" name="upload_excel"  action="<?php echo base_url(); ?>cardholder/import" class="" enctype="multipart/form-data">
          <input type="hidden" name="id" value="<?php echo $_SESSION['id']; ?>">
            <div class="form-group">
            <label class="form-lable">Excl File input :</label>
            <input type="file" name="file" id="file" required="">
                <p class="help-block primary">Select only CSV file extension's.</p>
       </div>
    
  
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" id="submit" name="import" class="btn btn-primary">Upload Data</button>
      </div>
        </form>
      </div>
    </div>
  </div>
</div>
                 
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    
                      <div class="row ">
                          <div class="col-xl-3 col-lg-4 col-md-3">
                              <div class="">
                              
                                <div class="card-block">
                                  <div class="card-body">
                                    <fieldset class="form-group  position-relative">
                                        <small class="text-muted primary" style="text-transform:uppercase;"> Search By Ref No </small>
                                        <input class="form-control" name="search_data" id="search_data" type="text" onkeyup="ajaxSearch();" placeholder="search">
                                        <div class="form-control-position " style="padding-top:16px;">
                                            <button class="btn btn-mda btn-primary" type="submit" name="submit" style="border-radius:50%; width:50px;height: 50px;"><i class="la la-search"></i></button>
                                       </div>
                                    </fieldset>
                                     
                                  </div>
                                </div>
                                    
                              </div>
                            </div>
                          <!--//second search box-->
                         <div class="col-xl-3 col-lg-4 col-md-3">
                              <div class="">
                                <div class="card-block">
                                  <div class="card-body">
                                    <fieldset class="form-group  position-relative">
                                       <small class="text-muted primary" style="text-transform:uppercase;"> Search by Barcode</small>
                                      <input class="form-control" name="search_barcode" id="search_barcode" type="text" onkeyup="ajaxSearchbarcode();" placeholder="search">
                                        <div class="form-control-position " style="padding-top:16px;">
                                            <button class="btn btn-mda btn-primary" type="submit" name="submit" style="border-radius:50%; width:50px;height: 50px;"><i class="la la-search"></i></button>
                                       </div>
                                    </fieldset>
                                  </div>
                                </div>
                              </div>
                            </div>
                          <!--//thired search box-->
                          <div class="col-xl-3 col-lg-4 col-md-3">
                              <div class="">
                                <div class="card-block">
                                  <div class="card-body">
                                    <fieldset class="form-group position-relative ">
                                      <small class="text-muted primary" style="text-transform:uppercase;"> search by Name</small>
                                      <input class="form-control" name="search_name" id="search_name" type="text" onkeyup="ajaxSearchname();" placeholder="search">
                                        <div class="form-control-position " style="padding-top:16px;">
                                            <button class="btn btn-mda btn-primary" type="submit" name="submit" style="border-radius:50%; width:50px;height: 50px;"><i class="la la-search"></i></button>
                                       </div>
                                    </fieldset>
                                  </div>
                                </div>
                              </div>
                            </div>
                          <div class="col-xl-3 col-lg-4 col-md-3">
                              <div class="">
                                <div class="card-block">
                                  <div class="card-body">
                                    <fieldset class="form-group position-relative">
                                       <small class="text-muted primary" style="text-transform:uppercase;"> search by Mobile No</small>
                                      <input class="form-control" name="search_mobile" id="search_mobile" type="text" onkeyup="ajaxSearchmobile();" placeholder="search">
                                       <div class="form-control-position " style="padding-top:16px;">
                                            <button class="btn btn-mda btn-primary" type="submit" name="submit" style="border-radius:50%; width:50px;height: 50px;"><i class="la la-search"></i></button>
                                       </div>
                                    </fieldset>
                                  </div>
                                </div>
                              </div>
                            </div>
                      </div>
                      <div class="card-text">
                          <?php if($feedback = $this->session->flashdata('feedback')):
			             $feedback_class = $this->session->flashdata('feedback_class');
	                         ?>
                      <div class="alert alert-icon-right <?php echo $feedback_class; ?> mb-2" role="alert">
                        <span class="alert-icon"><i class="la la-info"></i></span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                        <strong style="color:#fff;"> <?php echo $feedback; ?></strong>
                      </div>
                          <?php endif; ?>
                    </div>
                      <!--//table-->
                      <div id="suggestions" class="table-responsive">
                      <table id="" class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                            <th hidden="">#Id</th>
                            <th> संदर्भ क्रमांक </th>
                          <th> बारकोड क्रमाँक </th>
                          <th> कार्डधारक नाव </th>
                          <th> मोबाइल नंबर </th>
                          <th> कार्ड प्रकार </th>
                          <th > Unit</th>
                          <!--<th> तारीख </th>-->
                          <th>Mon.Repost</th>
                          <th>U.Action</th>
                        </tr>

                      </thead>
                      <tbody id="">
                           <?php if(count($cardholder) ): 
                                          $count=1;
			                  foreach($cardholder as $cardholder):?>
                          <tr style="font-size:13px;" id="autoSuggestionsList">
                            <td style="width: 2%" hidden=""><?php echo $count++; ?> </span></td>
                            <td> <span class="badge badge-pill badge-danger"><?php echo $cardholder->rel_id ?> </span></td>
                            <td> <?php echo $cardholder->barcode_no; ?> </td>
                            <td><?php echo $cardholder->c_name; ?></td>
                            <td><?php echo $cardholder->c_mobile; ?></td>
                         
                            <td><?php echo $cardholder->c_cardtype; ?></td>
                            <td>
                                <span class="badge badge-pill badge-primary" data-toggle="tooltip" data-placement="top" title="Total Unit">  <i class="la la-child" style="font-size:12px;" ></i> <?php echo $cardholder->adult; echo " + ". $cardholder->child;echo ' = ';  echo  $total=$cardholder->adult+$cardholder->child; ?> </span>
                                 <!--<span class="badge badge-info badge-pill" data-toggle="tooltip" data-placement="top" title="Child">     <i class="la la-child" style="font-size:12px;"></i> </span>-->
                                
                                </td>
                            <!--<td>ss</td>-->
<!--                            <td>
                                <?php  $dateform =  $cardholder->ccreate_at;
                                       $newDate = date("F j, Y", strtotime($dateform));
                                ?>
                                <span class="badge badge-pill badge-primary"><?php echo $newDate;?></span>
                            </td>-->
                            <td>
                               <div class="onoffswitch" id="<?php echo $cardholder->c_id; ?>">    
                                          <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox mstatus" id="myonoffswitch<?php echo $cardholder->c_id ; ?>" value="<?php echo $cardholder->mstatus; ?>" <?php echo $cardholder->mstatus == "1" ? 'checked' : '' ?> onclick="return jobchangeststus();" />
                                                 <label class="onoffswitch-label" for="myonoffswitch<?php echo $cardholder->c_id ; ?>" >
                                                       <span class="onoffswitch-inner"></span>
                                                       <span class="onoffswitch-switch"></span>
                                                   </label>
                                                   </div>
                            </td>
                            <td>
                                <span class="dropdown">
                                <button id="btnSearchDrop4" type="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="true" class="btn btn-primary dropdown-toggle dropdown-menu-right"><i class="ft-settings"></i></button>
                                <span aria-labelledby="btnSearchDrop4" class="dropdown-menu mt-1 dropdown-menu-right">
                                  <!--//view-->
                                  <a href="<?php echo base_url("cardholder/view_cardholder/$cardholder->c_id") ?>" class="dropdown-item"><i class="la la-eye"></i> View</a> 
                                  <a href="<?php echo base_url("cardholder/edit_cardholder/$cardholder->c_id") ?>" class="dropdown-item"><i class="ft-edit-2"></i> Edit</a>
                                   <?=	
				  form_open('cardholder/delete_cardholder'),
				  form_hidden('c_id',$cardholder->c_id),
                                  form_button(array('name' => 'submit', 'type' => 'submit', 'class' => 'dropdown-item','value'=>'', 'onclick'=>'return deleteemployee();','content' => '<i class="ft-trash-2"></i> delete')),
						form_close();
					      ?>
                                  <a href="<?php echo base_url("cardholder/add_family_members/$cardholder->c_id"); ?>" class="dropdown-item"><i class="ft-plus-circle primary"></i> Add Family</a>

                                </span>
                              </span>

                            </td>
                        </tr>
                          <?php endforeach; ?>
                          <?php else: ?>

                      <?php endif; ?>
                      </tbody>
                   
                    </table>
                      </div>
                      <!--//end table-->
                         </div>
                      </div>
                </div>
              </div>
            </div>
          </div>
        </section>
       <!--//end card body-->
        </div>
      <!--//end content wrapper-->  
    </div>

   <script>
    function addcardholder(){
    job=confirm("Are you sure you want to Add new Cardholder?");
    if(job!=true){
         document.location.reload(true);
        return false;
    }
   }
 </script>

      <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript">
        $(document).on("click",".mstatus",function(){
        var ida = $(this).parent().attr('id'); 
        window.alert(ida);
        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date+' '+time;
//        alert(dateTime);
       if($(this).prop("checked") == true){
              $.ajax({
                           type:"post",
                             url: "<?php echo site_url('cardholder/monthly_status/true');?>",
                            data:{c_id: ida,entry_date:dateTime},
                            success:function(data){
//                                alert('hii1');
//                               window.alert(data);
                               location.reload();
                                   }
                        });
                     }
                     else 
                            if($(this).prop("checked") == false){
                            $.ajax({
                                           type:'post',
                                           url: "<?php echo site_url('cardholder/monthly_status/false');?>",
                                          data:{c_id: ida,entry_date:dateTime},
                                          success:function(data){
//                                              alert('hii2');
//                                           window.alert(data);
                                          location.reload();
                                   }
                     });
                            }
                     });
     </script>
<?php require 'footer.php';?>
