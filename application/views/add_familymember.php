<?php require 'header.php';?>

<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0">Ration Card Holder</h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>cardholder"> Card Holder</a>
                </li>
                <li class="breadcrumb-item"><a href="#">New</a>
                </li>
                <li class="breadcrumb-item active">Add New Family Members
                </li>
              </ol>
            </div>
          </div>
        </div>
        
      </div>
        
         <div class="content-body">

        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                  <div class="card-header bg-panel" style="">
                    <h4 class="card-title">
                        <i class="la la-user-plus" ></i> || Add New Family Members
                    </h4>
                 </div>
                <div class="card-content collapse show">
                  <div class="card-body ">
                 <!--//body-->

         <?php echo form_open_multipart("cardholder/update_family_members/$cardholder->c_id") ?>
                      <div class="form-body">
                          <div class="col-md-9 offset-md-1">

                   <!--\\card repertr-->
                    <div class="repeater-default">
                      <div data-repeater-list="row" >
                        <div data-repeater-item>
                              
                         <div class="row "  >
                          <div class="form-group col-md-6 mb-2">
                            <label for="">Cardholder Family Member Name </label>
                            <div class="position-relative has-icon-left">
                                <input type="text" id="cm_name[]" class="form-control" required="" placeholder="Cardholder Family Member Name"  name="cm_name">
                              <div class="form-control-position">
                               <i class="la la-user"></i>
                              </div>
                            </div>
                          </div>
                          <div class="form-group col-md-4 mb-2">
                            <label for=""> Mobile No </label>
                            <div class="position-relative has-icon-left">
                                <input type="text"  id="cm_mobile[]" class="form-control" placeholder=" Mobile No "  name="cm_mobile">
                              <div class="form-control-position">
                               <i class="la la-tty"></i>
                              </div>
                            </div>
                          </div>
                                <div class="form-group col-sm-12 col-md-2 text-center mt-2">
                              <button type="button" class="btn btn-danger" data-repeater-delete> <i class="ft-x"></i> Delete</button>
                            </div>
                        </div>
                        </div>
                      </div>
                        <!--//add button-->
                        <div class="form-group overflow-hidden">
                        <div class="col-12">
                          <button data-repeater-create class="btn btn-primary">
                            <i class="ft-plus"></i> Add
                          </button>
                        </div>
                      </div>
                    </div>
                          
                          </div>
                        <!--//footer page-->
                        <div class="col-md-9 offset-1 ">   
                         <?php 
			      echo form_reset(array('name'=>'reset','value'=>'RESET','class'=>'btn btn-warning mr-1')),
			           form_submit(array('name'=>'submit','value'=>'ADD MEMBER','class'=>'btn btn-primary','onclick'=>' return addemployee();'));
			    ?>
                            
                      </div>
                            <?php form_close(); ?>
                  </div>
                 </div>
              </div>
            </div>
          </div>
        </section>
        </div>
    </div>
</div>
        
<?php require 'footer.php';?>