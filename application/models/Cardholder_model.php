<?php 
     if ( ! defined('BASEPATH')) exit('No direct script access allowed');
     
class Cardholder_model extends CI_Model{
         
        function __construct() 
         { 
          parent::__construct();
        }
    
        public function allactive_cardholder_list($from,$to) {
            
             if($from =='' && $to ==''){
                  $to=  date("Y-m-d");
                  $from= date("Y-04-01");
//                  var_dump($form);exit;
              } 
            $query = $this->db
		->select("C.*,")
		->from('cardholder_master C')
//                m_entry  
//                ->join('tbl_entry_' . date('m_Y', strtotime($from)) . ' M', 'C.c_id = M.c_id', 'join')
//                    ->where('M.m_entry')
//                ->join('tbl_entry_' . date('m_Y',strtotime($from)))
                ->order_by("C.c_id", "DESC")
		->get();
//            echo $this->db->last_query();exit;
		return $query->result();
            
        }
        public function add_cardholder_master($array) {
            $this->db->insert('cardholder_master',$array );
//           echo $this->db->last_query(); 
            return $this->db->insert_id();   
        }
        public function add_cardholder_deatils($array) {
          $this->db->insert('cardholder_details',$array );
           echo $this->db->last_query(); 
            return $this->db->insert_id();   
        }
        public function find_id_wise_cardholder($c_id) {
            $query = $this->db
		->select("cardholder_master.*")
		->from('cardholder_master')
                 ->where('cardholder_master.c_id', $c_id)
		->get();
              return $query->result(); 
        }
        public function find_id_wise_cardholder_family($c_id) {
          $query = $this->db
		->select("cardholder_details.*")
		->from('cardholder_details')
                 ->where('cardholder_details.c_id', $c_id)
		->get();
              return $query->result();  
        }
        public function find_cardholder($c_id) {
            $q = $this->db->select(array('c_id','user_id','rel_id','barcode_no','c_name','c_mobile','adult','child','c_cardtype','ccreate_at','	bnk_name','bnk_accno',''))
			      ->where('c_id',$c_id)
			      ->get('cardholder_master');
		return $q ->row();
            
        }
        public function update_cardholder($c_id,array $ch_up ) {
                 return $this->db
		    ->where('c_id',$c_id)
		     ->update('cardholder_master',$ch_up);   
        }
         public function update_cardholder_family($c_id,$ch_d ) {
             $id=$ch_d['cd_id'];
//             var_dump($id);exit;
                 return $this->db
		    ->where('c_id',$c_id)
                    ->where('cd_id',$id)
		     ->update('cardholder_details',$ch_d);   
        }
        public function delete_cardholder($c_id) {
         $this->db->delete('cardholder_master', array('c_id' => $c_id));   
        }
        
        public function insertCSV($data){
                $this->db->insert('cardholder_master', $data);
                return TRUE;
            }  
            
            
            public function fetch_data() {
              $this->db->select("cardholder_master.*,(SELECT(COUNT(cardholder_details.cm_name))
                              FROM cardholder_details 
                              WHERE cardholder_details.c_id = cardholder_master.c_id) AS row_num");
		$query = $this->db->get("cardholder_master");
//                echo $this->db->last_query();exit;
		return $query->result(); 
            }
            
       function is_referenceNO_available($rel_id) {  
           $this->db->where('rel_id', $rel_id);  
           $query = $this->db->get("cardholder_master"); 
//           echo $this->db->last_query();exit;
           if($query->num_rows() > 0)  
           {  
                return true;  
           }  
           else  
           {  
                return false;  
           }  
      }
      
      public function active_banklist() {
          $query = $this->db->query('SELECT bnk_name FROM  tbl_bank');
          return $query->result();  
      }
      
      
       function month_status($st){

           $today = date("Y-m-d");
           $data['c_id']=$_POST['c_id'];
           $data['entry_date']=$_POST['entry_date'];
           
           $this->session->set_userdata('userdata',$data);
           
    if ($st == "true"){ 
        echo "<script>alert('This card was not approved, Thanks.');</script>";
//         $this->db
//         ->set('mstatus', "0")
//         ->where('c_id', $_POST['c_id'])
//         ->update('cardholder_master');
//     echo"hi1";  echo $this->db->last_query();
       }else{
        $this->db
        ->set('mstatus', "0")
        ->where('c_id', $_POST['c_id'])
        ->update('cardholder_master');
  
      extract($this->session->userdata("userdata"));
      
            $query = "INSERT INTO `tbl_entry_" . date('m_Y',strtotime($today)) . "` VALUES ('','{$c_id}','{$m_entry}','{$entry_date}')";
            $this->db->query($query);
  
            $this->db
        ->set('m_entry', '1')
        ->where('c_id', $data['c_id'])
        ->update('tbl_entry_' . date('m_Y',strtotime($today)));
       }
    }
    
    //seearch
    
    public function backupdatasearch($reference_no) {

              return $this->db->where('rel_id',$reference_no)
                 ->select("cardholder_master.*")
		->from('cardholder_master')
                   
                      ->get()->result(); 
}


 public function get_autocomplete($search_data){
                $this->db->select('cardholder_master.*');
                $this->db->like('rel_id', $search_data);
                return $this->db->get('cardholder_master')->result();
        }
        
        public function is_bnk_name_available($bnk_name) {
         $this->db->where('bnk_name', $bnk_name);  
           $query = $this->db->get("tbl_bank"); 
//           echo $this->db->last_query();exit;
           if($query->num_rows() > 0)  
           {  
                return true;  
           }  
           else  
           {  
                return false;  
           }     
        }
        
        public function fetch_vdata($from, $to,$vendor) {
//                 $this->db->select("cardholder_master.*");
//		$query = $this->db->get("cardholder_master");
//                echo $this->db->last_query();exit;
//		return $query->result();  
            if ($from == '' && $to == '') {
            $to = date("Y-m-d");
            $from = date("Y-04-01");
        }  
        return $this->db
                    ->select('*')    
                    ->from('cardholder_master C')
                    ->join('tbl_entry_' . date('m_Y', strtotime($from)) . ' M', 'C.c_id = M.c_id', 'join')
                ->get()->result();
        }
}