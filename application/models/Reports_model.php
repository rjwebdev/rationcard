<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports_model extends CI_Model{
    function __construct() {
//        $this->userTbl = 'tbl_users';
    }
    
    public function monthly_visit_list($from, $to,$vendor) {
//        var_dump($to);
       if ($from == '' && $to == '') {
            $to = date("Y-m-d");
            $from = date("Y-04-01");
        }  
        return $this->db
                    ->select('*')    
                    ->from('cardholder_master C')
                    ->join('tbl_entry_' . date('m_Y', strtotime($from)) . ' M', 'C.c_id = M.c_id', 'join')
                   ->get()->result();               
    }
    
    public function monthly_visit_search($from,$to) {
//        var_dump($form);exit;
              if($from =='' && $to ==''){
                  $to=  date("Y-m-d");
                  $from= date("Y-04-01");
//                  var_dump($form);exit;
              } 
              
              return $this->db->where("entry_date BETWEEN '". date('Y-m-d',  strtotime($from))."' and'". date('Y-m-d',  strtotime($to))."' ")
//                 ->select("*")
		->from('cardholder_master C')
                ->join('tbl_entry_' . date('m_Y', strtotime($from)) . ' M', 'C.c_id = M.c_id', 'join')
                ->get()->result(); 
    }
    
    public function purchaselist($from, $to) {
//        var_dump($to);
//          var_dump($from);exit;
        if ($from == '' && $to =='') {
//            echo "h8" ;var_dump($from);exit;
            $to = date("Y-m-d");
            $from = date("Y-04-01");
        }
//        var_dump($to);exit;
//        $this->db->where('shopid', $this->session->userdata("shopid"));
//        
        return $this->db->where("entry_date BETWEEN '" . date('Y-m-d', strtotime($from)) . "' and '" . date('Y-m-d', strtotime($to)) . "'")
                        ->from('tbl_entry_' . date('m_Y', strtotime($from)) . ' S')
                        ->join('cardholder_master C', 'C.c_id = S.c_id', 'join')
                        ->get()->result();
                       echo $this->db->last_query();exit;
                
    }
}   