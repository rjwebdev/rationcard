<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model{
    function __construct() {
//        $this->userTbl = 'users';
         $this->load->database();
    }
    
    function get_user_count(){
	  $this->db->select("tbl_employee.emp_id");
	  $this->db->from("tbl_employee");
          $query = $this->db->get();
         return $query->num_rows();
	}
        
        public function get_carholder_count() {
          $this->db->select("cardholder_master.c_id");
	  $this->db->from("cardholder_master");
          $query = $this->db->get();
         return $query->num_rows(); 
        }
        
        public function active_recentlist() {
         $query = $this->db
		->select("*")
		->from('cardholder_master')
                ->order_by('cardholder_master.c_id', 'DESC')
                ->limit("5")
		->get();
		return $query->result();   
        }
        
        public function get_carholder_family_count() {
          $this->db->select("cardholder_details.cd_id");
	  $this->db->from("cardholder_details");
          $query = $this->db->get();
         return $query->num_rows(); 
        }
        
        public function monthly_visiter_count($from, $to) {
            
             if ($from == '' && $to == '') {
              $to = date("Y-m-d");
               $from = date("Y-04-01");
              } 
           $this->db->select("month_id");
	   $this->db->from('tbl_entry_' . date('m_Y', strtotime($from)));
           $query = $this->db->get();
           return $query->num_rows(); 
        }
        
        public function active_mobile_no() {
          $this->db->select("cardholder_details.cm_mobile");
	  $this->db->from("cardholder_details");
          $query = $this->db->get();
         return $query->num_rows();  
        }
}