<?php 
     if ( ! defined('BASEPATH')) exit('No direct script access allowed');
     
     class User_model extends CI_Model{
         
        function __construct() 
         { 
          parent::__construct();
        }
        public function active_users_list() {
           $query = $this->db
			 ->select("tbl_employee.*,tbl_role.*")
			 ->from('tbl_employee')
                           ->join ("tbl_role", "tbl_role.role_id = tbl_employee.eposition")
                         ->order_by('emp_id', 'desc')
			 ->get();
		return $query->result(); 
        }
        
        public function add_newuser($array) {
           $this->db->insert('tbl_employee',$array );
           return $this->db->insert_id(); 
        }
        public function add_logintable_data($array){
           $this->db->insert('tbl_users',$array );
           return $this->db->insert_id();  
        }
        
         public function find_user($emp_id){
		$q = $this->db->select('tbl_employee.*,tbl_role.*')
                               ->join ("tbl_role", "tbl_role.role_id = tbl_employee.eposition")
			      ->where('emp_id',$emp_id)
			      ->get('tbl_employee');
		return $q ->row();
	}
        
        public function update_employee($emp_id,array $employee){
	                return $this->db
				    ->where('emp_id',$emp_id)
			            ->update('tbl_employee',$employee);
	}
        public function update_loginuser_data($emp_id,array $a){
                         return $this->db
				    ->where('id',$emp_id)
			            ->update('tbl_users',$a);
        }
       
        public function delete_empl($emp_id){
             $this->db->delete('tbl_employee', array('emp_id' => $emp_id));
             $this->db->delete('tbl_users', array('id' => $emp_id));
	}
        
               
        public function get_role(){
            $query=  $this->db->get('tbl_role');
            if ($query->num_rows()>0){
                return $query->result();
            }
         }
        
     }
        