<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model{
    function __construct() {
//        $this->load->model('Welco');
        $this->userTbl = 'tbl_users';
    }

     public function insert($data = array()) {
  
        if(!array_key_exists("created_at", $data)){
            $data['created_at'] = date("Y-m-d H:i:s");
        }
        if(!array_key_exists("updated_at", $data)){
            $data['updated_at'] = date("Y-m-d H:i:s");
        }
        
        $insert = $this->db->insert($this->userTbl, $data);
        
        if($insert){
            return $this->db->insert_id();;
        }else{
            return false;
        }
    }
    
    function getRows($username,$password){
       $this->db->select('tbl_users.*,tbl_role.*'); 
       $this->db->from('tbl_users');
        $this->db->join('tbl_role', 'tbl_role.role_id= tbl_users.eposition');
      $this->db->where('username', $username);
      $this->db->where('password', $password);
      $query = $this->db->get();
//      echo $this->db->last_query();exit;
       return $query->result_array();
       
    }
    
    public function lastLoginInfo($id) {
     
      $this->db->select('tbl_last_login.loginDateTime');
      $this->db->join('tbl_users', 'tbl_users.id = tbl_last_login.id');
     
      $this->db->where('tbl_last_login.id', $id);
      $this->db->order_by('tbl_last_login.last_uid', 'DESC');
      $this->db->limit(1);
      $query = $this->db->get('tbl_last_login');
      
      return $query->result_array();
      
 }
    
    function lastLogin($loginInfo){
        $this->db->trans_start();
        $this->db->insert('tbl_last_login', $loginInfo);
        $this->db->trans_complete();
        
 }
    
    public function employee_all_details() {
      $query = $this->db
		->select("users.*,role.*")
		->from('users')
               ->join ("role", "role.role = users.role")
                 ->order_by('users.id','ASC')
	         ->get();
          return $query->row();
    }
    
    public function curd_list() {
      $query = $this->db
		->select("tbl_setting.*")
//               ->join ("tbl_setting", "tbl_setting.role = tbl_employee.role")
		->from('tbl_setting')
		->get();
		return $query->result();  
}


       public function saveNewPassword($newPassword){
           $old=$newPassword['old'];
//           var_dump($old);exit;
            $data = array(
                    'password' => $newPassword['new']
                    
                    );
            $this->db->where('id', $this->session->userdata('id'));
            $this->db->where('password',$old);
            $query = $this->db->update('tbl_users',$data);
           if($query){
//               echo "h12";
                return true;
           }else{
               echo "h22";
                return false;
           }
      }
      
      public function get_login_history() {
         
        $query = $this->db
		->select("tbl_last_login.*,users.*")
		->from('tbl_last_login')
               ->join('users', 'users.id = tbl_last_login.id')
                ->order_by("tbl_last_login.last_uid", "DESC")
		->get();
//                echo $this->db->last_query();exit;
		return $query->result();
      }
      
       function udab() {
        $datey = date("Y");
//        $datem = date("m_Y");
        $datemw = date("WY");
        $datey = date("Y");
        $datem = date("m_Y");
        $data = "CREATE TABLE IF NOT EXISTS `tbl_entry_" . $datem . "` (
                   `month_id` int(11) NOT NULL AUTO_INCREMENT,
                    `c_id` int(11) NOT NULL,
                    `m_entry` enum('1','0') NOT NULL DEFAULT '0',
                    `entry_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                   PRIMARY KEY (`month_id`)
                  ) Engine=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
         
               
        return $data;
    }
   
    
}
?>

