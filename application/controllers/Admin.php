<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
    
        function __construct() {
        parent::__construct();
        $this->load->library('user_agent');
        $this->load->library('form_validation');
        $this->load->model('Admin_model','admin');
        $this->load->helper('url');
        ob_start(); # add this
        $this->load->library('Session'); # add this
//        $ci =& get_instance();
//        $ci->load->helper('cias_helper');

    } 
    public function index() {
         $this->dbfunction();
         $returnval = 0;
       $data = array();
       
        if($this->session->userdata('success_msg')){
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
         //error msg show [error_msg]
        if($this->session->userdata('error_msg')){
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
             
        if($this->input->post('loginSubmit')){
            
            $this->form_validation->set_rules('username', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('password', 'password', 'required');
            
            if ($this->form_validation->run() == TRUE) {
                
                $username = $this->input->post('username');
                $password =md5($this->input->post('password'));
//                var_dump($password);exit;
                $checkLogin = $this->admin->getRows($username,$password);
//                var_dump($checkLogin['role_name']);exit;
                if($checkLogin == TRUE){
                    
                        foreach ($checkLogin  as $info)
                        {
                            $this->session->set_userdata('isUserLoggedIn',TRUE);
                            $this->session->set_userdata('userId',$checkLogin[0]['id']);
                            
                            $lastLogin = $this->admin->lastLoginInfo($info['id']);
                            
                            $sessionArray = array('id'=>$info['id'],
                                                    'username'=>$info['username'],
                                                     'name'=>$info['name'],
//                                                    'eposition'=>$info['eposition'],
                                                    'role_name'=>$info['role_name'],
                                                    'isLoggedIn' => TRUE
                                                );
//                                                var_dump($sessionArray);exit;
                            $this->session->set_userdata($sessionArray);
                            
                            unset($sessionArray->id, $sessionArray->isLoggedIn, $sessionArray->lastLogin);
                         
                            $loginInfo = array('id'=>$info['id'],
                                               'sessionData' => json_encode($sessionArray),
                                                'machine_ip' =>$_SERVER['REMOTE_ADDR'],
                                                'user_browser' => $this->agent->browser().':::'.$this->agent->version(),
                                                'browser_string' =>$this->agent->agent_string(),
                                                'platform' =>$this->agent->platform(),
//                                                 'test'=>$_SERVER['HTTP_USER_AGENT']

                                            );
//                                            var_dump($loginInfo);exit;
                            $this->admin->lastLogin($loginInfo);
                        }
                        $this->session->set_flashdata("success", "You are now logged in");
                        $_SESSION['isUserLoggedIn'] = TRUE;
                        $_SESSION['email'] = $email;
                        
                    redirect('home');
                }else{
                    $data['error_msg'] = 'Wrong email or password, please try again.';
                }
            }
        }
        //load the view
        $this->load->view('User_login', $data);
    }
    
    
    public function logout(){
        $this->session->unset_userdata('isUserLoggedIn');
        $this->session->unset_userdata('userId');
         $this->session->sess_destroy(); # Change
         $this->load->driver('cache'); # add
          $this->cache->clean();  # add
//           $this->session->set_userdata('success_msg', 'Your Logout was successfully. Please login to your account.');
        redirect('admin','refresh');
        ob_clean(); # add
    }
     
     public function user_profile() {
         
      if($this->session->userdata('isUserLoggedIn')){
             
         $this->load->view('profile');
      }else{
            $this->session->set_userdata('success_msg', 'Your Old Password Chnage successfully. Please login to your account.');
            redirect('admin');
      }
     }
     public function update() {
        
          if($this->session->userdata('isUserLoggedIn')){
         $this->load->helper(array('form','url'));
         $this->load->library('form_validation');

         $this->form_validation->set_rules('oldPassword','Old Password','required|callback_checkOldPassword');
         $this->form_validation->set_rules('newPassword','New Password','required|min_length[5]|max_length[30]');
         $this->form_validation->set_rules('renewPassword','Retype Password','required|matches[newPassword]');
         
         if($this->form_validation->run() == TRUE){
             
        $newPassword['new'] =md5($this->input->post('newPassword'));
        $newPassword['old'] =md5($this->input->post('oldPassword'));
         if($newPassword){
             $chnagepassword = $this->admin->saveNewPassword($newPassword);
             
               $this->session->set_flashdata('success_msg', 'Your Old Password Change successfully. Please login to your account.');
	       $this->session->set_flashdata('feedback_class','alert-success alert-dismissable fade in');
                 return redirect('admin');

            }else{
                $this->session->set_flashdata('feedback',"Old Password Change Failed..Plz Try Again");
		$this->session->set_flashdata('feedback_class','alert-danger');
            }
                 return redirect('admin/profile');
             
         }else{
        $this->load->view('profile');
     }
    }else{
      $this->session->set_userdata('success_msg', 'Your Logout was successfully. Please login to your account.');
    redirect('admin');
    } 
    }
    
    public function checkOldPassword($oldPassword){
        
    $this->db->select('tbl_users.*');
    $this->db->where('id',$this->session->userdata('id'));
    $this->db->where('password',md5($this->input->post('oldPassword')));
    $query1=$this->db->get('tbl_users');
    if($query1->num_rows()>0){
        return true;
    }else{
        $this->form_validation->set_message('checkOldPassword', 'wrong old password.');
        return false;
    }
}

function dbfunction() {
        $sqls = explode(';', $this->admin->udab());
        array_pop($sqls);
        foreach ($sqls as $statement) {
            $statment = $statement . ";";
            $this->db->query($statement);
        }
}


}