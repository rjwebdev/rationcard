<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
    
        function __construct() {
        parent::__construct();
        $this->load->library('user_agent');
        $this->load->library('form_validation');
        $this->load->model('User_model','user');
        $this->load->helper('url');
        ob_start(); # add this
        $this->load->library('Session'); # add this


    } 
    public function index() {
        if($this->session->userdata('isUserLoggedIn')){   
	  
           $admin_user = $this->user->active_users_list();
//         $userdata=  $this->admin->get_login_history(); 
           $this->load->view('users_list',array('admin_user'=>$admin_user));
           }else{
             $this->session->set_userdata('success_msg', 'Your Session is destroy. Please login to your account.');
             redirect('admin');
          }
      }
      
      
      public function add_employee(){
          if($this->session->userdata('isUserLoggedIn')){ 
              
                $this->load->model('user');
                $role = $this->user->get_role();
		$this->load->view('users_add',['role'=>$role]);
                
             }else{
             $this->session->set_userdata('success_msg', 'Your Session is destroy. Please login to your account.');
             redirect('admin');
          }
	}
        
       public function store_employee(){ 
            if($this->session->userdata('isUserLoggedIn')){
             $test = $this->input->post();
	    unset($test['submit']);
//            var_dump($test);exit;   
                         $a['emp_fname'] = $test['emp_fname'];
                         $a['emp_lname'] = $test['emp_lname'];
                         $a['emp_adds'] = $test['emp_adds'];
                         $a['emp_phone'] = $test['emp_phone'];
                         $a['eposition'] = $test['eposition'];
                         $a['emp_email'] = $test['emp_email'];    
                         $a['emp_pass']= md5($test['password']);
//                         var_dump($a);exit;
                        $emp_id=$this->user->add_newuser($a);
//                        var_dump($emp_id);exit;
                              $b['username'] = $test['emp_email'];
                              $b['password'] = md5($test['password']);
                              $b['name'] = $test['emp_fname'].''.$test['emp_lname'];;
                              $b['created_at'] = $test['created_on'];
                              $b['eposition'] = $test['eposition'];
//                              var_dump($b);exit;
                             if($b){
                                 $ac= $this->user->add_logintable_data($b);
                                 
				 $this->session->set_flashdata('feedback',"User Add Successfully..");
				 $this->session->set_flashdata('feedback_class','alert-info');  
			}
			else
			{
				$this->session->set_flashdata('feedback',"User Add Failed..Plz Try Again");
				$this->session->set_flashdata('feedback_class','alert-danger');
			}
                        return redirect('user');
                         }else{
                          $this->session->set_userdata('success_msg', 'Your Session is destroy. Please login to your account.');
                          redirect('admin');
                  }
		}
                
        public function edit_employee($emp_id){ 	
            
            if($this->session->userdata('isUserLoggedIn')){
            $user= $this->user->find_user($emp_id);
            $role = $this->user->get_role();
		$this->load->view('users_edit',array('user'=>$user,'role'=>$role));
                 }else{
             $this->session->set_userdata('success_msg', 'Your Session is destroy. Please login to your account.');
             redirect('admin');
          }

	}
        
        public function update_employee($emp_id){ 
            if($this->session->userdata('isUserLoggedIn')){

		$this->load->library('form_validation');
                          
			$post = $this->input->post();
			unset($post['submit']);

                        $a['eposition']=$post['eposition'];
                        $a['name']=$post['emp_fname'].''.$post['emp_lname'];
//                        var_dump($a);exit;
                       $b= $this->user->update_loginuser_data($emp_id,$a);
//                        var_dump( $a['eposition']);exit;
			if($this->user->update_employee($emp_id,$post))
			{
				$this->session->set_flashdata('feedback',"User Update Successfully..");
				$this->session->set_flashdata('feedback_class','alert-info');
			}
			else
			{
				$this->session->set_flashdata('feedback'," User Update Failed..Plz Try Again");
				$this->session->set_flashdata('feedback_class','alert-danger');
			}
                        
			return redirect('user');
                         }else{
             $this->session->set_userdata('success_msg', 'Your Session is destroy. Please login to your account.');
             redirect('admin');
          }
	}
        
          public function delete_employee(){
               if($this->session->userdata('isUserLoggedIn')){
		$emp_id = $this->input->post('emp_id');

		if($this->user->delete_empl($emp_id))
			{
				$this->session->set_flashdata('feedback',"User Delete Successfully..");
				$this->session->set_flashdata('feedback_class','alert-info');
			}
			else
			{
				$this->session->set_flashdata('feedback',"User Delete Successfully");
				$this->session->set_flashdata('feedback_class','alert-danger');
			}
			return redirect('user');
                         }else{
             $this->session->set_userdata('success_msg', 'Your Session is destroy. Please login to your account.');
             redirect('admin');
          }
	
     }
        
}