<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cardholder extends CI_Controller {

    public function __construct()
	{
		parent:: __construct();
                $this->load->helper('form');
                $this->load->library('form_validation');
                $this->load->library('session');
                 $this->load->library('excel');
                $this->load->helper('url');
                $this->load->helper('csv');
                $this->load->helper('form');
                $this->load->helper('url');
                 $this->load->database();
		$this->load->model("Cardholder_model",'cardholder');
	}

        public function index() {
            if($this->session->userdata('isUserLoggedIn')){
           
                $data['cardholder']=$this->cardholder->allactive_cardholder_list(date('Y-m-1'), date('Y-m-d'),'');
//                var_dump($data['cardholder']);exit;
            $this->load->view('cardholder_list',$data);
            }else{
            $this->session->set_userdata('success_msg', 'Your Session is destroy. Please login to your account.');
            redirect('admin');
        }
     }
        
        public function add_cardholder() {
            if($this->session->userdata('isUserLoggedIn')){
//                echo "hii1";exit;
              $data['bankname']=  $this->cardholder->active_banklist();
            $this->load->view('cardholder_add',$data); 
          }else{
            $this->session->set_userdata('success_msg', 'Your Session is destroy. Please login to your account.');
            redirect('admin');
        }
     }
     
     public function store_cardholder() {
        if($this->session->userdata('isUserLoggedIn')){
//             var_dump($_POST['row']);exit;
//            var_dump($_POST);exit;
//                        var_dump($field_values_array);exit;
            $this->load->library('form_validation');
            $test = $this->input->post();
	     unset($test['submit']); 
             // bank new chack
              if ($test['bnk_name'] == "New") {
//                 echo "hi";
                     $this->db->insert('tbl_bank', array('bnk_name' => $test['banknameN']));
                      $test['bnk_name'] = $test['banknameN'];
                     } 
//                     var_dump($test['bnk_name']);exit;
               $ch['user_id'] = $test['id'];
               $ch['rel_id'] = $test['rel_id'];
               $ch['c_name'] = $test['c_name'];
               $ch['barcode_no'] = $test['barcode_no'];
               $ch['c_mobile'] = $test['c_mobile'];
               $ch['bnk_name'] = $test['bnk_name'];
               $ch['bnk_accno'] = $test['bnk_accno'];
               $ch['c_cardtype'] = $test['c_cardtype'];
               $ch['adult']=$test['adult'];
               $ch['child']=$test['child'];
               $ch['ccreate_at'] = $test['ccreate_at'];

               $c_masterid=$this->cardholder->add_cardholder_master($ch);
//               var_dump($c_masterid);exit;
                 	
                        $field_values_array = $_POST['row'];
//                       print_r($field_values_array); 
                       foreach($field_values_array as $value){
                            $ch_d['c_id']=$c_masterid;
                            $ch_d['cm_name']=$value['cm_name'];
                            $ch_d['cm_mobile']=$value['cm_mobile'];
                  if($ch_d){
                    $this->cardholder->add_cardholder_deatils($ch_d);
                                                                  
	                  $this->session->set_flashdata('feedback',"Cardholder Add Successfully..");
			  $this->session->set_flashdata('feedback_class','alert-info');
			}else{
		           $this->session->set_flashdata('feedback',"cardholder Add Failed..Plz Try Again");
			   $this->session->set_flashdata('feedback_class','alert-danger');
			}
        }
                return redirect('cardholder');       
           }else{
            $this->session->set_userdata('success_msg', 'Your Session is destroy. Please login to your account.');
             redirect('admin'); 
       }
 }
 
      public function view_cardholder($c_id) {
           if($this->session->userdata('isUserLoggedIn')){
               
           $cardholdr_details['cardholder_data']= $this->cardholder->find_id_wise_cardholder($c_id);
           $cardholdr_details['cardholder_details']= $this->cardholder->find_id_wise_cardholder_family($c_id);
               
          $this->load->view('cardholder_view',$cardholdr_details);     
          }else{
               $this->session->set_userdata('success_msg', 'Your Session is destroy. Please login to your account.');
                redirect('admin'); 
          }
    }
 
     public function edit_cardholder($c_id) {
             if($this->session->userdata('isUserLoggedIn')){
              $cardholder= $this->cardholder->find_cardholder($c_id);
              $bankname=  $this->cardholder->active_banklist();
               $cardholder_details= $this->cardholder->find_id_wise_cardholder_family($c_id);
              $this->load->view('cardholder_edit',array('cardholder'=>$cardholder,'bankname'=>$bankname,'cardholder_details'=>$cardholder_details));
          }else{
            $this->session->set_userdata('success_msg', 'Your Session is destroy. Please login to your account.');
             redirect('admin'); 
       }
 }
 
     public function update_cardholder($c_id) {
        
      if($this->session->userdata('isUserLoggedIn')){
       $this->load->library('form_validation');
//       
        $test = $this->input->post();
        unset($test['submit']);
        
//        var_dump($test);exit;
//         var_dump($_POST);exit;
//               $ch['user_id'] = $test['id'];
               $ch_up['rel_id'] = $test['rel_id'];
               $ch_up['c_name'] = $test['c_name'];
               $ch_up['barcode_no'] = $test['barcode_no'];
               $ch_up['c_mobile'] = $test['c_mobile'];
               $ch_up['bnk_name'] = $test['bnk_name'];
               $ch_up['bnk_accno'] = $test['bnk_accno'];
               $ch_up['c_cardtype'] = $test['c_cardtype'];
               $ch_up['adult']=$test['adult'];
               $ch_up['child']=$test['child'];
               $ch_up['update_on'] = $test['update_on'];
//               var_dump($ch_up);exit;
               $true=$this->cardholder->update_cardholder($c_id,$ch_up);
//               var_dump($true);exit;
               
//               $data=array(
                   $data['c_id'] = $c_id;
                   $data['cd_id'] = $this->input->post('cd_id');
                   $data['cm_name'] = $this->input->post('cm_name');
                   $data['cm_mobile'] =$this->input->post('cm_mobile');
//               );
//               var_dump($data);exit;
               for($i=0;$i < count($data['c_id']);$i++){
//                   echo"hii1";exit;
                $batch[]= array('c_id'=> $data['c_id'][$i],
                                 'cd_id'=> $data['cd_id'][$i],
                               'cm_name'=>$data['cm_name'][$i],
                               'cm_mobile'=>$data['cm_mobile'][$i]
                    );
               }
               foreach($batch as $value){
                            $ch_d['cd_id']=$value['cd_id'];
                            $ch_d['cm_name']=$value['cm_name'];
                            $ch_d['cm_mobile']=$value['cm_mobile'];
                            
                   $TRUE=$this->cardholder->update_cardholder_family($c_id,$ch_d);
//               var_dump($batch[]['cd_id']);exit;
//              $aaaa= $this->cardholder->update_cardholder_family($c_id,$batch);
//               var_dump($ch_d);exit;
                 if($TRUE){
//                 $this->cardholder->update_cardholder_family($c_id,$ch_d);
               
		   $this->session->set_flashdata('feedback',"Cardholder Update Successfully..");
		   $this->session->set_flashdata('feedback_class','alert-info');
                }else{
		    $this->session->set_flashdata('feedback',"Cardholder Update Failed..Plz Try Again");
	            $this->session->set_flashdata('feedback_class','alert-danger');
			}
                       }
                        return redirect('cardholder');
                    
        }else{
            $this->session->set_userdata('success_msg', 'Your Session is destroy. Please login to your account.');
        redirect('admin');
        
        }
     }
     
     
     public function add_family_members($c_id) {
         if($this->session->userdata('isUserLoggedIn')){
         $cardholder= $this->cardholder->find_cardholder($c_id);
         $this->load->view('add_familymember',['cardholder'=>$cardholder]);
         }else{
            $this->session->set_userdata('success_msg', 'Your Session is destroy. Please login to your account.');
             redirect('admin'); 
       }
     }
     
     public function update_family_members($c_id) {
        if($this->session->userdata('isUserLoggedIn')){
            
         $this->load->library('form_validation');
//       
        $test = $this->input->post();
        unset($test['submit']); 
        
                      $field_values_array = $_POST['row'];
                       print_r($field_values_array); 
                       foreach($field_values_array as $value){
                            $ch_d['c_id']=$c_id;
                            $ch_d['cm_name']=$value['cm_name'];
                            $ch_d['cm_mobile']=$value['cm_mobile'];
                           
                  if($ch_d){
                    $this->cardholder->add_cardholder_deatils($ch_d);
                                                                  
	                  $this->session->set_flashdata('feedback',"Cardholder New Family Member Add Successfully..");
			  $this->session->set_flashdata('feedback_class','alert-info');
			}else{
		           $this->session->set_flashdata('feedback',"Cardholder New Family Member Add Failed..Plz Try Again");
			   $this->session->set_flashdata('feedback_class','alert-danger');
			}
        }
                return redirect('cardholder');       
           }else{
            $this->session->set_userdata('success_msg', 'Your Session is destroy. Please login to your account.');
             redirect('admin'); 
       }
 }
     
     public function delete_cardholder(){
               if($this->session->userdata('isUserLoggedIn')){
		$c_id = $this->input->post('c_id');

		if($this->cardholder->delete_cardholder($c_id))
			{
				$this->session->set_flashdata('feedback',"Cardholder Delete Successfully..");
				$this->session->set_flashdata('feedback_class','alert-info');
			}
			else
			{
				$this->session->set_flashdata('feedback',"Cardholder Delete Successfully");
				$this->session->set_flashdata('feedback_class','alert-danger');
			}
			return redirect('rationcardholder');
                        }else{
           $this->session->set_userdata('success_msg', 'Your Session is destroy. Please login to your account.');
            redirect('admin');
	}
     }
     
     public function import() {
        if($this->session->userdata('isUserLoggedIn')){
          
            if(isset($_POST["import"])){
                 $filename=$_FILES["file"]["tmp_name"];
      
                 if($_FILES["file"]["size"] > 0){
                    $file = fopen($filename, "r");
                    
           while (($importdata = fgetcsv($file, 10000, ",")) !== FALSE){
              
                  $data = array(
                      'rel_id' => $importdata[0],
                      'barcode_no' =>$importdata[1],
                      'c_name'=>$importdata[2],
                      'c_mobile'=>$importdata[3],
                       'c_unit'=>$importdata[4],
                       'c_cardtype'=>$importdata[5],
                       'user_id'=>$_SESSION['id'],
                      'ccreate_at' => date('Y-m-d H:i:s'),
                      );
//                      var_dump($data);exit;
              $insert = $this->cardholder->insertCSV($data);
           } 
           
          fclose($file);
          $this->session->set_flashdata('feedback',"Nam Data are imported successfully..");
          $this->session->set_flashdata('feedback_class','alert-info');

                  redirect('rationcardholder');
                 }else{
                $this->session->set_flashdata('feedback',"Something went wrong..");
		$this->session->set_flashdata('feedback_class','alert-danger');
                    redirect('rationcardholder');
                 }
            } 
        }else{
           $this->session->set_userdata('success_msg', 'Your Session is destroy. Please login to your account.');
            redirect('admin');
	}      
     }
     
     public function export(){
          if($this->session->userdata('isUserLoggedIn')){
          $object = new PHPExcel();
          $object->setActiveSheetIndex(0);
      
          $table_columns = array("संदर्भ क्रमांक", "बारकोड क्रमाँक", "कार्डधारक नाव","मोबाइल नंबर","प्रौढ","बालक","कुटुंब सदस्य मोजणी","कार्ड प्रकार","तारीख");

         $column = 0;
         foreach($table_columns as $field){
                 $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
                 $column++;
      }

       
       $cardholder_data = $this->cardholder->fetch_data();
//       var_dump($cardholder_data);exit;
       $excel_row = 2;

       foreach($cardholder_data as $row){

            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->rel_id);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->barcode_no);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->c_name);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->c_mobile);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->adult/2);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->child);
            $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->row_num);
            $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->c_cardtype);
            $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->ccreate_at);
             $excel_row++;
       }

       $object_writer = PHPExcel_IOFactory::createwriter($object, 'Excel5');

       header('Content-Type: application/vnd.ms-excel');
       header('Content-Disposition: attachment;filename="Ration card holder data.xls"');
       header('Cache-Control: max-age=0');

       $object_writer->save('php://output');  
     
        }else{
           $this->session->set_userdata('success_msg', 'Your Session is destroy. Please login to your account.');
            redirect('admin');
	}
       }
       //stop working

        
        public function monthly_status($par = NULL) {
          if($this->session->userdata('isUserLoggedIn')){

         $this->cardholder->month_status($par); 
           }else{
           $this->session->set_userdata('success_msg', 'Your Session is destroy. Please login to your account.');
            redirect('admin');
	}  
        }
        
        //new search option here
        

        
        
       public function autocomplete(){
                // load model
               $this->load->model("Cardholder_model",'cardholder');
                $search_data = $this->input->post('search_data');
//                var_dump($search_data);exit;
                $result = $this->cardholder->get_autocomplete($search_data);
//                var_dump($result);exit;
                if (!empty($result))
                {
                     $count=1;
                    foreach ($result as $cardholder):
                     echo "<tr style='font-size:13px;' id='autoSuggestionsList'>
                            <td  hidden=''><?php echo $count++; ?> </span></td>
                            <td> <span class='badge badge-pill badge-danger'><?php echo $cardholder->rel_id ?> </span></td>
                            <td> <?php echo $cardholder->barcode_no; ?> </td>
                            <td><?php echo $cardholder->c_name; ?></td>
                            <td><?php echo $cardholder->c_mobile; ?></td>
                         
                            <td><?php echo $cardholder->c_cardtype; ?></td>
                            <td>
                                <span class='badge badge-pill badge-primary' data-toggle='tooltip' data-placement='top' title='Adult'>  <i class='la la-mars-stroke' style='font-size:12px;' ></i> <?php echo $cardholder->adult/2 ?> </span>
                                 <span class='badge badge-info badge-pill' data-toggle='tooltip' data-placement='top' title='Child'>     <i class='la la-child' style='font-size:12px;'></i> <?php echo $cardholder->child ?></span>
                                
                                </td>
       
                            <td>
                               <div class='onoffswitch' id='<?php echo $cardholder->c_id; ?>'>    
                                          <input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox mstatus' id='myonoffswitch' value='<?php echo '$cardholder->mstatus;' ?>' <?php echo $cardholder->mstatus == '0' ? 'checked' : '' ?> onclick='return jobchangeststus();' />
                                                 <label class='onoffswitch-label' for='myonoffswitch<?php echo $cardholder->c_id; ?>' >
                                                       <span class='onoffswitch-inner'></span>
                                                       <span class='onoffswitch-switch'></span>
                                                   </label>
                                                   </div>
                            </td>
                            <td>
                                <span class='dropdown'>
                                <button id='btnSearchDrop4' type='button' data-toggle='dropdown' aria-haspopup='true'
                                aria-expanded='true' class='btn btn-primary dropdown-toggle dropdown-menu-right'><i class='ft-settings'></i></button>
                                <span aria-labelledby='btnSearchDrop4' class='dropdown-menu mt-1 dropdown-menu-right'>
                                  <!--//view-->
                                  <a href='<?php echo base_url('cardholder/view_cardholder/$cardholder->c_id') ?>' class='dropdown-item'><i class='la la-eye'></i> View</a> 
                                  <a href='<?php echo base_url('cardholder/edit_cardholder/$cardholder->c_id') ?>' class='dropdown-item'><i class='ft-edit-2'></i> Edit</a>
                                   <?=	
				  form_open('cardholder/delete_cardholder'),
				  form_hidden('c_id',$cardholder->c_id),
                                  form_button(array('name' => 'submit', 'type' => 'submit', 'class' => 'dropdown-item','value'=>'', 'onclick'=>'return deleteemployee();','content' => '<i class='ft-trash-2'></i> delete')),
						form_close();
					      ?>
                                  <a href='<?php echo base_url('cardholder/add_family_members/$cardholder->c_id'); ?>' class='dropdown-item'><i class='ft-plus-circle primary'></i> Add Family</a>

                                </span>
                              </span>

                            </td>
                        </tr>";
                        
                        
                    endforeach;
                }
                else
                {
                    echo "<li> <em> Not found ... </em> </li>";
                }
        }
        
        public function check_bnk_name_avalibility($par = NULL) {
            if($this->session->userdata('isUserLoggedIn')){
           
                 if(!($_POST["bnk_name"])){  
                    echo '<label class="text-danger"><span class="glyphicon glyphicon-remove"></span> Invalid</span></label>';  
                    }else{  
                    if($this->cardholder->is_bnk_name_available($_POST["bnk_name"])){  
                     echo '<label class="text-danger"><span class="glyphicon glyphicon-remove"></span> Bank name Already Available</label>';  
                   }else{  
                     echo '<label class="text-success"><span class="glyphicon glyphicon-ok"></span> New Create Bank Name </label>';  
                }  
           } 
                
           }else{
           $this->session->set_userdata('success_msg', 'Your Session is destroy. Please login to your account.');
            redirect('admin');
	}
        }
        
         public function vexport() {
             if($this->session->userdata('isUserLoggedIn')){
             $object = new PHPExcel();
          $object->setActiveSheetIndex(0);
      
          $table_columns = array("संदर्भ क्रमांक", "बारकोड क्रमाँक", "कार्डधारक नाव","मोबाइल नंबर","कार्ड प्रकार","तारीख");

         $column = 0;
         foreach($table_columns as $field){
                 $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
                 $column++;
      }

       
         $cardholder_data = $this->cardholder->fetch_vdata(date('Y-m-1'), date('Y-m-d'),'');
//       var_dump($cardholder_data);exit;
       $excel_row = 2;

       foreach($cardholder_data as $row){

            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->rel_id);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->barcode_no);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->c_name);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->c_mobile);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->c_cardtype);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->entry_date);
             $excel_row++;
       }

       $object_writer = PHPExcel_IOFactory::createwriter($object, 'Excel5');

       header('Content-Type: application/vnd.ms-excel');
       header('Content-Disposition: attachment;filename="Ration card holder data.xls"');
       header('Cache-Control: max-age=0');

       $object_writer->save('php://output');  
     
        }else{
           $this->session->set_userdata('success_msg', 'Your Session is destroy. Please login to your account.');
            redirect('admin');
	}
        }
}