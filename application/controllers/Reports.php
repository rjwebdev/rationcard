<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

    public function __construct()
	{
		parent:: __construct();
                $this->load->helper('form');
                $this->load->library('form_validation');
                $this->load->library('session');
                 $this->load->library('excel');
                $this->load->helper('url');
                $this->load->helper('csv');
                $this->load->helper('form');
                $this->load->helper('url');
                 $this->load->database();
		$this->load->model("Reports_model",'report');
	}

        public function index() {
            if($this->session->userdata('isUserLoggedIn')){
            $data['monthly_visit'] = $this->report->monthly_visit_list(date('Y-m-1'), date('Y-m-d'),'');
//            var_dump($data);exit;
             
//            if($this->input->post()){
////                  var_dump($_POST);exit;
//                extract($this->input->post());
////                var_dump($_POST);exit;
//                $data['monthly_visit'] = $this->report->monthly_visit_search($fromDate,$toDate);
////                var_dump( $data['backups']);exit;
//              }else{
//                 $data['monthly_visit'] = $this->report->monthly_visit_list(date('Y-04-01'), date('Y-m-d'),'');
//              }
            
            $this->load->view('reports_data',$data);
            }else{
            $this->session->set_userdata('success_msg', 'Your Session is destroy. Please login to your account.');
            redirect('admin');
        }
     }
     
     public function purchase_Report() {
         
//      $data['lists'] = $this->report->monthly_visit_list(date('Y-m-1'), date('Y-m-d'),''); 
//      var_dump($data)
        if ($this->input->post()) {
//            var_dump($_POST);exit;
        extract($this->input->post());
          echo "h1";
            $data['lists'] = $this->report->purchaselist($fromdate, $todate);
//            var_dump( $data['lists']);exit;
        } else {
              echo "h2";
            $data['lists'] = $this->report->purchaselist(date("Y-m-1"), date("Y-m-d"), '');
//            var_dump( $data['lists']);exit;
        }
//        var_dump($data['lists']);exit;
//        $this->load->view('header', $data);
        $this->load->view('purchase-report',$data);
    }
       
}