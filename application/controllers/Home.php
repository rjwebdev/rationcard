<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
    
        function __construct() {
        parent::__construct();
        $this->load->library('user_agent');
        $this->load->library('form_validation');
        $this->load->model('Home_model','home');
        $this->load->helper('url');
        ob_start(); # add this
        $this->load->library('Session'); # add this


    } 
    public function index() {
         if($this->session->userdata('isUserLoggedIn')){
          $data['user_count'] = $this->home->get_user_count();
          $data['carholder_count'] = $this->home->get_carholder_count();
          $data['carholder_familymembers'] = $this->home->get_carholder_family_count();
          $data["recent_data"]=$this->home->active_recentlist(); 
          $data["monthly_visiter"]=$this->home->monthly_visiter_count(date('Y-m-1'), date('Y-m-d'),''); 
          $data["mobile_no"]=$this->home->active_mobile_no();
          $this->load->view('dashboard',$data);
            
            }else{
           $data['error_msg'] = 'your session destroy plz login agin.';
            redirect('admin',$data);
        }   
    }
 
}